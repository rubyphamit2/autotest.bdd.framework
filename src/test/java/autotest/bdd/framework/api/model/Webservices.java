
package autotest.bdd.framework.api.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "webservice" })
@XmlRootElement(name = "webservices")
public class Webservices {
	private List<Webservice> webservice;

	public Webservices() {
		super();
	}

	public List<Webservice> getWebservice() {
		if (webservice == null) {
			webservice = new ArrayList<>();
		}
		return webservice;
	}

	public void setWebservice(List<Webservice> webservice) {
		this.webservice = webservice;
	}

}
