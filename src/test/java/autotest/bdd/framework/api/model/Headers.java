
package autotest.bdd.framework.api.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "headers", propOrder = { "header" })
public class Headers {
	private List<String> header;

	public Headers() {
		super();
	}

	public List<String> getHeader() {
		if (header == null) {
			header = new ArrayList<>();
		}
		return header;
	}

	public void setHeader(List<String> header) {
		this.header = header;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (String header : getHeader()) {
			builder.append(header).append("&");
		}
		return builder.toString();
	}

}
