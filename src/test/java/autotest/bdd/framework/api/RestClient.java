
package autotest.bdd.framework.api;

import java.net.URI;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.NTCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.AuthSchemes;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.glassfish.jersey.apache.connector.ApacheClientProperties;
import org.glassfish.jersey.apache.connector.ApacheConnectorProvider;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.client.HttpUrlConnectorProvider;
import org.glassfish.jersey.client.RequestEntityProcessing;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.internal.util.collection.MultivaluedStringMap;

public class RestClient {
	private static final String SSL_TYPE = "TLSv1";
	private static int CONNECT_TIMEOUT = 30 * 1000;
	private static int READ_TIMEOUT = 30 * 1000;
	private Client client;
	private static RestClient restClient = null;
	private HostnameVerifier allHostsValid;
	private SSLContext sc;

	public static RestClient getInstance() {
		if (restClient == null) {
			restClient = new RestClient();
		}
		return restClient;
	}

	private RestClient() {
		try {
			sc = SSLContext.getInstance(SSL_TYPE);// Java 8
			System.setProperty("https.protocols", SSL_TYPE);// Java 8

			TrustManager[] trustAllCerts = { new InsecureTrustManager() };
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			allHostsValid = new InsecureHostnameVerifier();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Response doGet(String host, String path, String queryParams, String headers, String body, String username, String password,
			String authMode) throws Exception {
		String url = buildTarget(host, path, queryParams);

		buildClient(url, username, password, authMode);

		final Response response = client.target(url).request().property(ClientProperties.CONNECT_TIMEOUT, CONNECT_TIMEOUT)
				.property(ClientProperties.READ_TIMEOUT, READ_TIMEOUT).property(ClientProperties.FOLLOW_REDIRECTS, true)
				.headers(buildMultivaluedMap(headers)).get();
		return response;
	}

	public Response doDelete(String host, String path, String queryParams, String headers, String body, String username, String password,
			String authMode) throws Exception {
		String url = buildTarget(host, path, queryParams);

		buildClient(url, username, password, authMode);

		final Response response = client.target(url).request().property(ClientProperties.CONNECT_TIMEOUT, CONNECT_TIMEOUT)
				.property(ClientProperties.READ_TIMEOUT, READ_TIMEOUT).property(ClientProperties.FOLLOW_REDIRECTS, true)
				.headers(buildMultivaluedMap(headers)).delete();
		return response;
	}

	public Response doPost(String host, String path, String queryParams, String headers, String body, String username, String password,
			String authMode, String mediaType) {

		if (body == null || mediaType == null) {
			return null;
		}

		try {
			String url = buildTarget(host, path, queryParams);
			buildClient(url, username, password, authMode);
			final Response response = client.target(url).request(mediaType).property(ClientProperties.CONNECT_TIMEOUT, CONNECT_TIMEOUT)
					.property(ClientProperties.READ_TIMEOUT, READ_TIMEOUT).property(ClientProperties.FOLLOW_REDIRECTS, true)
					.headers(buildMultivaluedMap(headers)).post(buildEntity(body, mediaType), Response.class);
			return response;
		} catch (Exception e) {
			return null;
		}

	}

	public Response doPut(String host, String path, String queryParams, String headers, String body, String username, String password,
			String authMode, String mediaType) throws Exception {

		if (body == null || mediaType == null) {
			return null;
		}
		String url = buildTarget(host, path, queryParams);

		buildClient(url, username, password, authMode);

		final Response response = client.target(url).request(mediaType).property(ClientProperties.CONNECT_TIMEOUT, CONNECT_TIMEOUT)
				.property(ClientProperties.READ_TIMEOUT, READ_TIMEOUT).property(ClientProperties.FOLLOW_REDIRECTS, true)
				.headers(buildMultivaluedMap(headers)).put(buildEntity(body, mediaType), Response.class);
		return response;
	}

	public Response doPatch(String host, String path, String queryParams, String headers, String body, String username, String password,
			String authMode, String mediaType) throws Exception {

		if (body == null || mediaType == null) {
			return null;
		}
		String url = buildTarget(host, path, queryParams);

		buildClient(url, username, password, authMode);

		final Response response = client.target(url).request(mediaType).property(HttpUrlConnectorProvider.SET_METHOD_WORKAROUND, true)
				.property(ClientProperties.CONNECT_TIMEOUT, CONNECT_TIMEOUT).property(ClientProperties.READ_TIMEOUT, READ_TIMEOUT)
				.property(ClientProperties.FOLLOW_REDIRECTS, true).headers(buildMultivaluedMap(headers))
				.method("PATCH", buildEntity(body, mediaType), Response.class);
		return response;
	}

	private Entity buildEntity(String body, String mediaType) {
		Entity entity = Entity.text("");
		if (body != null && !body.isEmpty() && mediaType != null) {
			switch (mediaType) {
			case MediaType.APPLICATION_JSON:
				entity = Entity.json(body);
				break;
			case MediaType.APPLICATION_XML:
				entity = Entity.xml(body);
				break;
			case MediaType.APPLICATION_FORM_URLENCODED:
				entity = Entity.form(buildForm(body));
				break;
			default:
				entity = Entity.text(body);
				break;
			}
		}
		return entity;
	}

	public String buildTarget(String host, String path, String queryParams) {
		StringBuilder sb = new StringBuilder();
		sb.append(host);
		if (path != null && !path.isEmpty()) {
			sb.append("/");
			sb.append(path);
		}

		if (queryParams != null && !queryParams.isEmpty()) {
			sb.append("?");
			sb.append(queryParams);
		}
		return sb.toString();
	}

	public void close() {
		if (client != null) {
			client.close();
		}
	}

	private void buildClient(String url, String username, String password, String authMode) {
		ClientConfig clientConfig = new ClientConfig();
		client = ClientBuilder.newBuilder().withConfig(clientConfig).sslContext(sc).hostnameVerifier(allHostsValid).build();

		if (username != null && !username.isEmpty() && password != null) {
			if (AuthSchemes.NTLM.equals(authMode)) {
				clientConfig.property(ClientProperties.REQUEST_ENTITY_PROCESSING, RequestEntityProcessing.BUFFERED);
				clientConfig.property(ApacheClientProperties.CREDENTIALS_PROVIDER,
						buildCredentialsProvider(URI.create(url), username, password));
				clientConfig.connectorProvider(new ApacheConnectorProvider());
			} else {
				client.register(HttpAuthenticationFeature.basic(username, password));
			}
		}
	}

	private CredentialsProvider buildCredentialsProvider(URI uri, String username, String password) {
		CredentialsProvider credentialsProvider = new BasicCredentialsProvider();

		/* NTLM auth for on premise installation */

		String domain = null;
		int idx = Math.max(username.indexOf('/'), username.indexOf('\\'));
		if (idx > 0) {
			domain = username.substring(0, idx);
			username = username.substring(idx + 1);
		}

		credentialsProvider.setCredentials(new AuthScope(uri.getHost(), uri.getPort(), AuthScope.ANY_REALM, AuthSchemes.NTLM),
				new NTCredentials(username, password, uri.getHost(), domain));
		return credentialsProvider;
	}

	private Form buildForm(String params) {
		Form form = new Form();
		if (params == null || params.isEmpty()) {
			return form;
		}

		String[] arr;
		String[] a;
		arr = params.split("&");
		for (int i = 0; i < arr.length; i++) {
			a = arr[i].split("=");
			if (a.length > 1) {
				form.param(a[0], a[1]);
			}
		}
		return form;
	}

	public static MultivaluedMap buildMultivaluedMap(String params) {
		MultivaluedMap multivaluedMap = new MultivaluedStringMap();

		if (params == null || params.trim().isEmpty()) {
			return multivaluedMap;
		}

		String[] arr;
		String[] a = new String[2];
		arr = params.split("&");
		int index = 0;
		for (int i = 0; i < arr.length; i++) {
			index = arr[i].indexOf("=");
			if (index > 0) {
				a[0] = arr[i].substring(0, index);
				a[1] = arr[i].substring(index + 1, arr[i].length());
				multivaluedMap.add(a[0], a[1]);
			}
		}
		return multivaluedMap;
	}

	// abc ==abc -> abc = abc

	public class InsecureHostnameVerifier implements HostnameVerifier {
		@Override
		public boolean verify(String hostname, SSLSession session) {
			return true;
		}
	}

	public class InsecureTrustManager implements X509TrustManager {
		/**
		 * {@inheritDoc}
		 */
		@Override
		public void checkClientTrusted(final X509Certificate[] chain, final String authType) throws CertificateException {
			// Everyone is trusted!
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void checkServerTrusted(final X509Certificate[] chain, final String authType) throws CertificateException {
			// Everyone is trusted!
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public X509Certificate[] getAcceptedIssuers() {
			return new X509Certificate[0];
		}
	}
}
