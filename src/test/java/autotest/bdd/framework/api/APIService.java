
package autotest.bdd.framework.api;

import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import autotest.bdd.framework.utils.AppContext;
import autotest.bdd.framework.utils.ConstUtils;
import autotest.bdd.framework.utils.Utils;

public class APIService {
	protected static final Logger LOGGER = LogManager.getLogger(APIService.class);

	public String requestWebServiceREST(String host, String path, String queryParams, String headers, String body, String username,
			String password, String authMode, String mediaType, String method) {
		String result = null;
		Response response = null;
		try {
			LOGGER.debug("calling REST service: HOST={} PATH={} QUERIES={} HEADERS={} BODY={} MEDIATYPE ={} METHOD = {}", host, path,
					queryParams, headers, body, mediaType, method);

			switch (method) {
			case HTTPMethod.GET:
				response = webServiceDoGet(host, path, queryParams, headers, body, username, password, authMode);
				break;
			case HTTPMethod.POST:
				response = webServiceDoPost(host, path, queryParams, headers, body, username, password, authMode, mediaType);
				break;
			case HTTPMethod.PUT:
				response = webServiceDoPut(host, path, queryParams, headers, body, username, password, authMode, mediaType);
				break;
			case HTTPMethod.DELETE:
				response = webServiceDoDelete(host, path, queryParams, headers, body, username, password, authMode);
				break;
			default:
				break;
			}

			if (response != null) {
				result = response.readEntity(String.class);
				AppContext.get().setCacheData(ConstUtils.CACHED_API_RESULT_ENTITY, result);

				AppContext.get().setCacheData(ConstUtils.CACHED_API_RESULT_JSON_DOCUMENT, Utils.parseJsonToDoc(result));
				AppContext.get().setCacheData(ConstUtils.CACHED_API_RESULT_HTTP_CODE, response.getStatus());
			} else {
				throw new AssertionError("API call: Response is null");
			}
		} catch (Exception e) {
			throw new AssertionError("API call error: " + e.getMessage());
		} finally {
			RestClient.getInstance().close();
		}

		return result;
	}

	/**
	 * @param host
	 * @param path
	 * @param queryParams
	 * @param headers
	 * @param body
	 * @param username
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public Response webServiceDoGet(String host, String path, String queryParams, String headers, String body, String username,
			String password, String authMode) throws Exception {
		return RestClient.getInstance().doGet(host, path, queryParams, headers, body, username, password, authMode);
	}

	/**
	 * @param host
	 * @param path
	 * @param queryParams
	 * @param headers
	 * @param body
	 * @param username
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public Response webServiceDoDelete(String host, String path, String queryParams, String headers, String body, String username,
			String password, String authMode) throws Exception {
		return RestClient.getInstance().doDelete(host, path, queryParams, headers, body, username, password, authMode);
	}

	/**
	 * @param host
	 * @param path
	 * @param queryParams
	 * @param headers
	 * @param body
	 * @param username
	 * @param password
	 * @param mediaType
	 * @return
	 * @throws Exception
	 */
	public Response webServiceDoPost(String host, String path, String queryParams, String headers, String body, String username,
			String password, String authMode, String mediaType) throws Exception {
		return RestClient.getInstance().doPost(host, path, queryParams, headers, body, username, password, authMode, mediaType);
	}

	/**
	 * @param host
	 * @param path
	 * @param queryParams
	 * @param headers
	 * @param body
	 * @param username
	 * @param password
	 * @param mediaType
	 * @return
	 * @throws Exception
	 */
	public Response webServiceDoPut(String host, String path, String queryParams, String headers, String body, String username,
			String password, String authMode, String mediaType) throws Exception {
		return RestClient.getInstance().doPut(host, path, queryParams, headers, body, username, password, authMode, mediaType);
	}
}
