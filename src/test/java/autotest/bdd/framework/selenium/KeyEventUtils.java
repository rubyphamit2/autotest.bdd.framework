
package autotest.bdd.framework.selenium;

import java.awt.event.KeyEvent;

public class KeyEventUtils {

	public static int getKeyCode(String keyName) {
		try {
			return KeyEvent.class.getField("VK_" + keyName.toUpperCase()).getInt(null);
		} catch (Exception e) {
			e.printStackTrace();

		}
		return -1;
	}

}
