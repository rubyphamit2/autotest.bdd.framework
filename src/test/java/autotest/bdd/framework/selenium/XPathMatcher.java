
package autotest.bdd.framework.selenium;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class XPathMatcher {
	private static final Logger LOGGER = LogManager.getLogger(XPathMatcher.class);

	private String xpath;

	private XPathMatcher(String selector) {
		this.xpath = selector;
	}

	public static XPathMatcher xpath() {
		return XPathMatcher.xpath("descendant-or-self::*");
	}

	protected static XPathMatcher xpath(String selector) {
		return new XPathMatcher(selector);
	}

	public static XPathMatcher innerXpath() {
		return new XPathMatcher("");
	}

	/**
	 * 
	 * Selects all ancestors (parent, grandparent, etc.) of the current node "
	 * * @param condition
	 * 
	 * @return
	 */
	public XPathMatcher ancestor() {
		this.xpath = this.xpath.concat("/ancestor::*");
		return this;
	}

	/**
	 * 
	 * Selects all ancestors (parent, grandparent, etc.) of the current node and
	 * the current node itself " * @param condition
	 * 
	 * @return
	 */
	public XPathMatcher ancestorOrSelf() {
		this.xpath = this.xpath.concat("/ancestor-or-self::*");
		return this;
	}

	/**
	 * 
	 * Selects all attributes of the current node " * @param condition
	 * 
	 * @return
	 */
	public XPathMatcher attribute() {
		this.xpath = this.xpath.concat("/attribute::*");
		return this;
	}

	/**
	 * 
	 * Selects all children of the current node " * @param condition
	 * 
	 * @return
	 */
	public XPathMatcher child() {
		this.xpath = this.xpath.concat("/child::*");
		return this;
	}

	/**
	 * 
	 * Selects all descendants (children, grandchildren, etc.) of the current
	 * node " * @param condition
	 * 
	 * @return
	 */
	public XPathMatcher descendant() {
		this.xpath = this.xpath.concat("/descendant::*");
		return this;
	}

	/**
	 * 
	 * Selects all descendants (children, grandchildren, etc.) of the current
	 * node and the current node itself " * @param condition
	 * 
	 * @return
	 */
	public XPathMatcher descendantOrSelf() {
		this.xpath = this.xpath.concat("/descendant-or-self::*");
		return this;
	}

	/**
	 * 
	 * Selects everything in the document after the closing tag of the current
	 * node " * @param condition
	 * 
	 * @return
	 */
	public XPathMatcher following() {
		this.xpath = this.xpath.concat("/following::*");
		return this;
	}

	/**
	 * 
	 * Selects all siblings after the current node " * @param condition
	 * 
	 * @return
	 */
	public XPathMatcher followingSibling() {
		this.xpath = this.xpath.concat("/following-sibling::*");
		return this;
	}

	/**
	 * 
	 * Selects all namespace nodes of the current node " * @param condition
	 * 
	 * @return
	 */
	public XPathMatcher namespace() {
		this.xpath = this.xpath.concat("/namespace::*");
		return this;
	}

	/**
	 * 
	 * Selects the parent of the current node " * @param condition
	 * 
	 * @return
	 */
	public XPathMatcher parent() {
		this.xpath = this.xpath.concat("/parent::*");
		return this;
	}

	/**
	 * 
	 * Selects all nodes that appear before the current node in the document,
	 * except ancestors, attribute nodes and namespace nodes " * @param
	 * condition
	 * 
	 * @return
	 */
	public XPathMatcher preceding() {
		this.xpath = this.xpath.concat("/preceding::*");
		return this;
	}

	/**
	 * 
	 * Selects all siblings before the current node " * @param condition
	 * 
	 * @return
	 */
	public XPathMatcher precedingSibling() {
		this.xpath = this.xpath.concat("/preceding-sibling::*");
		return this;
	}

	/**
	 * 
	 * Selects the current node
	 * 
	 * @return
	 */
	public XPathMatcher self() {
		this.xpath = this.xpath.concat("/self::*");
		return this;
	}

	/**
	 * 
	 * Selects nodes with condition all ancestors (parent, grandparent, etc.) of
	 * the current node " *
	 * 
	 * @return
	 */
	public XPathMatcher hasAncestor() {
		this.xpath = this.xpath.concat("ancestor::*");
		return this;
	}

	/**
	 * 
	 * Selects nodes with condition all ancestors (parent, grandparent, etc.) of
	 * the current node and the current node itself " *
	 * 
	 * 
	 * @return
	 */
	public XPathMatcher hasAncestorOrSelf() {
		this.xpath = this.xpath.concat("ancestor-or-self::*");
		return this;
	}

	/**
	 * 
	 * Selects nodes with condition all attributes of the current node " *
	 * 
	 * 
	 * @return
	 */
	public XPathMatcher hasAttribute() {
		this.xpath = this.xpath.concat("attribute::*");
		return this;
	}

	/**
	 * 
	 * Selects nodes with condition all children of the current node " *
	 * 
	 * @param condition
	 * 
	 * @return
	 */
	public XPathMatcher hasChild() {
		this.xpath = this.xpath.concat("child::*");
		return this;
	}

	/**
	 * 
	 * Selects nodes with condition all descendants (children, grandchildren,
	 * etc.) of the current node " *
	 * 
	 * 
	 * @return
	 */
	public XPathMatcher hasDescendant() {
		this.xpath = this.xpath.concat("descendant::*");
		return this;
	}

	/**
	 * 
	 * Selects nodes with condition all descendants (children, grandchildren,
	 * etc.) of the current node and the current node itself " *
	 * 
	 * 
	 * @return
	 */
	public XPathMatcher hasDescendantOrSelf() {
		this.xpath = this.xpath.concat("descendant-or-self::*");
		return this;
	}

	/**
	 * 
	 * Selects nodes with condition everything in the document after the closing
	 * tag of the current node " *
	 * 
	 * 
	 * @return
	 */
	public XPathMatcher hasFollowing() {
		this.xpath = this.xpath.concat("following::*");
		return this;
	}

	/**
	 * 
	 * Selects nodes with condition all siblings after the current node " *
	 * 
	 * 
	 * @return
	 */
	public XPathMatcher hasFollowingSibling() {
		this.xpath = this.xpath.concat("following-sibling::*");
		return this;
	}

	/**
	 * 
	 * Selects nodes with condition all namespace nodes of the current node " *
	 * 
	 * @param condition
	 * 
	 * @return
	 */
	public XPathMatcher hasNamespace() {
		this.xpath = this.xpath.concat("namespace::*");
		return this;
	}

	/**
	 * 
	 * Selects nodes with condition the parent of the current node " *
	 * 
	 * 
	 * @return
	 */
	public XPathMatcher hasParent() {
		this.xpath = this.xpath.concat("parent::*");
		return this;
	}

	/**
	 * 
	 * Selects nodes with condition all nodes that appear before the current
	 * node in the document, except ancestors, attribute nodes and namespace
	 * nodes " *
	 * 
	 * 
	 * @return
	 */
	public XPathMatcher hasPreceding() {
		this.xpath = this.xpath.concat("preceding::*");
		return this;
	}

	/**
	 * 
	 * Selects nodes with condition all siblings before the current node " *
	 * 
	 * 
	 * @return
	 */
	public XPathMatcher hasPrecedingSibling() {
		this.xpath = this.xpath.concat("preceding-sibling::*");
		return this;
	}

	/**
	 * 
	 * @param attribute
	 *            class, arial-checked, aria-hidden, style, ...
	 * @param text
	 * @return
	 */
	public XPathMatcher hasAttributeEquals(String attribute, String text) {
		this.xpath = this.xpath.concat("[@").concat(attribute).concat("=\"").concat(text).concat("\"]");
		return this;
	}

	public XPathMatcher hasAttributeContains(String attribute, String text) {
		this.xpath = this.xpath.concat("[contains(@").concat(attribute).concat(",\"").concat(text).concat("\")]");

		return this;
	}

	public XPathMatcher hasClassEquals(String text) {
		return hasAttributeEquals("class", text);
	}

	public XPathMatcher hasClassContains(String text) {
		return hasAttributeContains("class", text);
	}

	public XPathMatcher hasAttributeNotEmpty(String attribute) {
		this.xpath = this.xpath.concat("[string-length(@").concat(attribute).concat(")!=0]");

		return this;
	}

	public XPathMatcher hasTextEquals(String text) {
		return hasTextEquals(text, false);
	}

	public XPathMatcher hasTextEquals(String text, boolean whiteSpaceSensitive) {
		if (whiteSpaceSensitive) {
			this.xpath = this.xpath.concat("[text()=\"").concat(text).concat("\"]");
		} else {
			this.xpath = this.xpath.concat("[normalize-space(text())=\"").concat(text).concat("\"]");

		}
		return this;

	}

	public XPathMatcher hasTextContains(String text, boolean whiteSpaceSensitive) {
		if (whiteSpaceSensitive) {
			this.xpath = this.xpath.concat("[contains(text(),\"").concat(text).concat("\")]");

		} else {
			this.xpath = this.xpath.concat("[contains(normalize-space(text()),\"").concat(text).concat("\")]");
		}
		return this;
	}

	public XPathMatcher hasTextContains(String text) {

		return hasTextContains(text, false);
	}

	public XPathMatcher hasIndex(int index) {
		this.xpath = this.xpath.concat("[" + index + "]");
		return this;
	}

	public XPathMatcher hasCondition(String condition) {
		this.xpath = this.xpath.concat("[").concat(condition).concat("]");
		return this;
	}

	public XPathMatcher hasCondition(XPathMatcher condition) {
		this.xpath = this.xpath.concat("[").concat(condition.getXPath()).concat("]");
		return this;
	}

	public String getXPath() {
		return this.xpath;
	}

	public XPathMatcher withTagName(String name) {
		char lastChar = this.xpath.charAt(this.xpath.length() - 1);
		if (lastChar == '*') {
			this.xpath = this.xpath.substring(0, this.xpath.length() - 1);
		}
		this.xpath = this.xpath.concat(name);
		return this;
	}

}
