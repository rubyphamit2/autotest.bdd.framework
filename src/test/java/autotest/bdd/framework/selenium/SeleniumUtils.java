
package autotest.bdd.framework.selenium;

import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;

import autotest.bdd.framework.base.EnvironmentProperties;

public class SeleniumUtils {
	private static final Logger LOGGER = LogManager.getLogger(SeleniumUtils.class);
	private static final Properties OBJECT_REPOSITORY = EnvironmentProperties.OBJECT_REPOSITORY;

	private static String getPathWithParams(String objPath, String... params) {
		return String.format(objPath, params);
	}
	
	public static String getObjPath(String objPath) {
		return OBJECT_REPOSITORY.getProperty(objPath);
	}

	public static String getObjPath(String objPath, String... params) {
		return getPathWithParams(objPath, params);

	}

	public static By getBy(String objPath, String... params) {

		By r = null;
		try {
			if (OBJECT_REPOSITORY.getProperty(objPath + ".Id") != null) {
				r = By.id(getPathWithParams(OBJECT_REPOSITORY.getProperty(objPath + ".Id"), params));
			} else if (OBJECT_REPOSITORY.getProperty(objPath + ".Name") != null) {
				r = By.name(getPathWithParams(OBJECT_REPOSITORY.getProperty(objPath + ".Name"), params));
			} else if (OBJECT_REPOSITORY.getProperty(objPath + ".TagName") != null) {
				r = By.tagName(getPathWithParams(OBJECT_REPOSITORY.getProperty(objPath + ".TagName"), params));
			} else if (OBJECT_REPOSITORY.getProperty(objPath + ".LinkText") != null) {
				r = By.linkText(getPathWithParams(OBJECT_REPOSITORY.getProperty(objPath + ".LinkText"), params));
			} else if (OBJECT_REPOSITORY.getProperty(objPath + ".PartialLinkText") != null) {
				r = By.partialLinkText(getPathWithParams(OBJECT_REPOSITORY.getProperty(objPath + ".PartialLinkText"), params));
			} else if (OBJECT_REPOSITORY.getProperty(objPath + ".CssSelector") != null) {
				r = By.cssSelector(getPathWithParams(OBJECT_REPOSITORY.getProperty(objPath + ".CssSelector"), params));
			} else if (OBJECT_REPOSITORY.getProperty(objPath + ".ClassName") != null) {
				r = By.className(getPathWithParams(OBJECT_REPOSITORY.getProperty(objPath + ".ClassName"), params));
			} else if (OBJECT_REPOSITORY.getProperty(objPath + ".XPath") != null) {
				r = By.xpath(
						XPathMatcher.xpath().getXPath() + getPathWithParams(OBJECT_REPOSITORY.getProperty(objPath + ".XPath"), params));
			} else if (OBJECT_REPOSITORY.getProperty(objPath) != null) {
				r = By.id(getPathWithParams(OBJECT_REPOSITORY.getProperty(objPath), params));
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
		LOGGER.debug("Checking locator " + r);
		return r;
	}

}
