package autotest.bdd.framework.steps.basic.mobile.ios;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.jbehave.core.annotations.Then;
import org.openqa.selenium.WebElement;

import autotest.bdd.framework.model.IOSWidget;
import autotest.bdd.framework.utils.Utils;

public class IOSBaseVerificationSteps extends IOSBaseActionSteps {
	/****************************************************************************************************
	 * VERIFY IOS ELEMENT EXIST OR NOT
	 ******************************************************************************************************/

	@Then("I should see id '$id' displays in IOSApp")
	public void verifyElementWithIdExist(String id) {
		WebElement webElement = getElementWithId(id);
		verifyExist(id, webElement);
	}

	@Then("I should see the '$type' name '$text' displays in IOSApp")
	public void verifyWidgetWithTextExist(String type, String text) {
		WebElement webElement = getWidgetWithText(type, text);
		verifyExist(type, webElement);
	}

	@Then("I should see the '$type' contains '$text' displays in IOSApp")
	public void verifyWidgetWithTextContainsExist(String type, String text) {
		WebElement webElement = getWidgetWithTextContains(type, text);
		verifyExist(type, webElement);
	}

//	@Then("I should see '$type' id '$id' displays in IOSApp")
//	public void verifyWidgetWithIdExist(String type, String id) {
//		WebElement webElement = getWidgetWithId(type, id);
//		verifyExist(type + " " + id, webElement);
//	}

	@Then("I should not see id '$id' in IOSApp")
	public void verifyElementWithIdNotExist(String id) {
		WebElement webElement = getElementWithId(id);
		verifyNotExist(id, webElement);
	}

	@Then("I should not see the '$type' name '$text' in IOSApp")
	public void verifyWidgetWithTextNotExist(String type, String text) {
		WebElement webElement = getWidgetWithText(type, text);
		verifyNotExist(type, webElement);
	}

//	@Then("I should not see '$type' id '$id' in IOSApp")
//	public void verifyWidgetWithIdNotExist(String type, String id) {
//		WebElement webElement = getWidgetWithId(type, id);
//		verifyNotExist(type + " " + id, webElement);
//	}
//	
//	@Then("I should see '$type' id '$id' No.$index in IOSApp")
//	public void verifyWidgetWithIdExist(String type, String id, int index) {
//		WebElement webElement = getWidgetWithIdAt(type, id,index);
//		verifyExist(type + " " + id, webElement);
//	}
//	

	/****************************************************************************************************
	 * VERIFY IOS ELEMENT VALUE
	 ******************************************************************************************************/

	@Then("I should see id '$id' is '$value' in IOSApp")
	public void verifyElementWithIdValue(String id, String value) {
		WebElement webElement = getElementWithId(id);
		assertEquals(value, getText(webElement));
	}

	@Then("I should see id '$id' contains '$value' in IOSApp")
	public void verifyElementWithIdContainsValue(String id, String value) {
		WebElement webElement = getElementWithId(id);
		verifyContains(value, getText(webElement));
	}

	@Then("I should see id '$id' ends '$value' in IOSApp")
	public void verifyElementWithIdEndsWithValue(String id, String value) {
		WebElement webElement = getElementWithId(id);
		verifyEndsWith(value, getText(webElement));
	}

	@Then("I should see the '$type' is '$value' in IOSApp")
	public void verifyWidgetValue(String type, String value) {
		WebElement webElement = getWidget(type);
		assertEquals(value, getText(webElement));
	}

//	@Then("I should see '$type' id '$id' is '$value' in IOSApp")
//	public void verifyWidgetWithIdValue(String type, String id, String value) {
//		WebElement webElement = getWidgetWithId(type, id);
//		assertEquals(value, getText(webElement));
//	}
//
//	@Then("I should see '$type' id '$id' contains '$value' in IOSApp")
//	public void verifyWidgetWithIdContainsValue(String type, String id, String value) {
//		WebElement webElement = getWidgetWithId(type, id);
//		verifyContains(value, getText(webElement));
//	}
//
//	@Then("I should see '$type' id '$id' ends '$value' in IOSApp")
//	public void verifyWidgetWithIdEndsWithValue(String type, String id, String value) {
//		WebElement webElement = getWidgetWithId(type, id);
//		verifyEndsWith(value, getText(webElement));
//	}
//
//	@Then("I should see '$type' id '$id' state is $state in IOSApp")
//	public void verifyWidgetWithIdState(String type, String id, String state) {
//		WebElement webElement = getWidgetWithId(type, id);
//		verifyWidgetState(IOSWidget.getIOSWidget(type), type + " " + id, webElement, state);
//	}

	@Then("I should see '$type' name '$text' state is $state in IOSApp")
	public void verifyWidgetWithTextState(String type, String text, String state) {
		WebElement webElement = getWidgetWithText(type, text);
		verifyWidgetState(IOSWidget.getIOSWidget(type), type + " " + text, webElement, state);
	}

//	@Then("I should see '$type' id '$id' state isn't $state in IOSApp")
//	public void verifyWidgetWithIdNotState(String type, String id, String state) {
//		WebElement webElement = getWidgetWithId(type, id);
//		verifyWidgetNotState(IOSWidget.getIOSWidget(type), type + " " + id, webElement, state);
//	}

	@Then("I should see '$type' name '$text' state isn't $state in IOSApp")
	public void verifyWidgetWithTextNotState(String type, String text, String state) {
		WebElement webElement = getWidgetWithText(type, text);
		verifyWidgetNotState(IOSWidget.getIOSWidget(type), type + " " + text, webElement, state);
	}

//	@Then("I should see '$type' id '$id' No.$index in IOSApp is '$value'")
//	public void verifyWidgetWithIdExist(String type, String id, int index, String value) {
//		WebElement webElement = getWidgetWithIdAt(type, id,index);
//		String actual = getText(webElement);
//		assertEquals(value, actual);
//	}

	private void verifyWidgetState(IOSWidget type, String name, WebElement webElement, String state) {
		String actual = getAttribute(webElement, state);
		boolean actualState = Utils.getBoolValue(actual);
		assertFalse(name + " should not be " + state, actualState);
	}

	private void verifyWidgetNotState(IOSWidget type, String name, WebElement webElement, String state) {
		String actual = getAttribute(webElement, state);
		boolean actualState = Utils.getBoolValue(actual);
		assertFalse(name + " should not be " + state, actualState);
	}

}
