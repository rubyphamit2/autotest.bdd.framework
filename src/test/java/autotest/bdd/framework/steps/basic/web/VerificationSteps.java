
package autotest.bdd.framework.steps.basic.web;

import org.assertj.core.api.Assertions;
import org.jbehave.core.annotations.Named;

public class VerificationSteps extends WebBaseSteps {

	public void verifyCurrentUrl(@Named("url") String url) {
		Assertions.assertThat(getCurrentUrl()).isEqualTo(url);
	}
}
