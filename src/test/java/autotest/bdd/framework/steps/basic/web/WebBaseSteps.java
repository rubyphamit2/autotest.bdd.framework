
package autotest.bdd.framework.steps.basic.web;

import java.util.ArrayList;
import java.util.List;

import org.jbehave.core.annotations.When;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import autotest.bdd.framework.steps.basic.BaseSteps;
import autotest.bdd.framework.utils.AppContext;
import autotest.bdd.framework.utils.ConstUtils;
import net.thucydides.core.webdriver.WebDriverFacade;

public class WebBaseSteps extends BaseSteps {
	public void launch(String path) {
		getPage().openAt(AppContext.get().getCurrentUrl() + path);

	}

	public String getCurrentUrl() {
		return getPage().getDriver().getCurrentUrl().replace(AppContext.get().getCurrentUrl(), "");
	}

	@When("I refresh the page")
	public void refreshPage() {
		getPage().getDriver().navigate().refresh();
	}

	@When("I locate to '$url'")
	public void navigate(String url) {
		launch(url);
	}

	@When("I close the browser")
	public void resetDriver() {
		((WebDriverFacade) getPage().getDriver()).reset();
	}

	public void openNewTab() {
		((JavascriptExecutor) getPage().getProxiedDriver()).executeScript("window.open()");
	}

	public void closeCurrentTab() {
		Keys key = null;
		if (ConstUtils.isMac) {
			key = Keys.COMMAND;
		} else {
			key = Keys.CONTROL;
		}

		WebElement body = getPage().getProxiedDriver().findElement(By.xpath("//body"));
		if (body != null) {
			body.sendKeys(key + "w");
		}
	}

	public void switchToTab(int index) {
		List<String> tabs = new ArrayList<String>(getPage().getDriver().getWindowHandles());
		getPage().getDriver().switchTo().window(tabs.get(index - 1));
	}

}
