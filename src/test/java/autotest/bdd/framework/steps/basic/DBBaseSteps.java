
package autotest.bdd.framework.steps.basic;

import static org.junit.Assert.assertTrue;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.jbehave.core.annotations.When;

import com.microsoft.sqlserver.jdbc.SQLServerException;

import autotest.bdd.framework.db.DBService;
import autotest.bdd.framework.utils.ConstUtils;

public class DBBaseSteps extends BaseSteps {
	public ResultSet dbExecuteQuery(String sql, boolean isFieldData) {
		LOGGER.debug("sql='" + sql + "'");
		ResultSet result = null;
		try {
			result = DBService.getInstance().executeQuery(sql);
		} catch (SQLServerException ex) {
			if (!"The statement did not return a result set.".equals(ex.getMessage())) {
				LOGGER.debug("error='" + ex.getMessage() + "'");
				assertTrue("Execute db failed", false);
			}
		} catch (Exception ex) {
			LOGGER.debug("error='" + ex.getMessage() + "'");
			assertTrue("Execute db failed", false);
		}

		storeData(ConstUtils.CACHED_DB_RESULT, result);
		return result;
	}

	@When("I store total records drom db as db field 'DB Total Records'")
	public void storeTotalRecord() {
		int totalRecord = 0;

		try {
			ResultSet resultSet = getCachedResultSet();
			totalRecord = resultSet.getRow();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		storeField(totalRecord, "DB Total Records");
	}

	@When("I store '$value' drom db as db field '$dbField'")
	public void storeField(String value, String dbField) {
		storeData(ConstUtils.CACHED_DB_FIELD_PREFIX + dbField, value);
	}

	public void storeField(int value, String dbField) {
		storeData(ConstUtils.CACHED_DB_FIELD_PREFIX + dbField, value);
	}

	private ResultSet getCachedResultSet() {
		return (ResultSet) getCacheData(ConstUtils.CACHED_DB_RESULT);
	}

	public void dbDisconnect() {
		DBService.getInstance().disconnect();
	}
}
