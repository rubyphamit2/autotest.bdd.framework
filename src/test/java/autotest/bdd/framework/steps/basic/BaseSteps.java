package autotest.bdd.framework.steps.basic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.awt.Robot;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import com.google.common.collect.Ordering;

import autotest.bdd.framework.base.MyPageObject;
import autotest.bdd.framework.driver.MyDriver;
import autotest.bdd.framework.selenium.KeyEventUtils;
import autotest.bdd.framework.selenium.SeleniumUtils;
import autotest.bdd.framework.utils.AppContext;
import autotest.bdd.framework.utils.ConstUtils;
import autotest.bdd.framework.utils.Utils;

public class BaseSteps {
	protected static final Logger LOGGER = LogManager.getLogger(BaseSteps.class);

	protected String[] pathParams = null;

	protected MyPageObject baseScreen;

	protected final MyPageObject getPage() {
		return baseScreen;
	}

	/****************************************************************************************************
	 * ACTION BASE STEP
	 ******************************************************************************************************/

	protected boolean scrollTo(WebElement webElement) {
		boolean r = false;
		try {
			((JavascriptExecutor) getPage().getDriver())
					.executeScript("arguments[0].scrollIntoView(true); window.scrollBy(0, -window.innerHeight / 4);", webElement);
			r = true;

		} catch (Exception e) {
		}
		return r;
	}

	protected void clickWithoutScroll(WebElement button) {
		clickWithoutScroll("Element", button);
	}

	protected void click(WebElement button) {
		click("Element", button);
	}

	protected void doubleClick(WebElement button) {
		doubleClick("Element", button);
	}

	protected void click(String eleName, WebElement button) {
		assertNotNull(eleName + " should be in the view.", button);
		scrollTo(button);
		button.click();
		getPage().waitFor(500);
	}

	protected void doubleClick(String eleName, WebElement button) {
		assertNotNull(eleName + " should be in the view.", button);
		scrollTo(button);
		button.click();
		try {
			button.click();
		} catch (Exception e) {
		}
		getPage().waitFor(500);
	}

	protected void clickWithoutScroll(String eleName, WebElement button) {
		assertNotNull(eleName + " should be in the view.", button);
		button.click();
		getPage().waitFor(500);
	}

	protected void sendKeys(WebElement webElement, String data) {
		try {
			clear(webElement);
			webElement.sendKeys(data);
		} catch (Exception e) {
			setElementValue(webElement, data);
		}
	}

	protected void sendKeys(WebElement webElement, int data) {
		try {
			clear(webElement);
			webElement.sendKeys("" + data);
		} catch (Exception e) {
			setElementValue(webElement, data);
		}
	}

	protected void clear(WebElement webElement) {
		// click(webElement);
		webElement.sendKeys(Keys.chord(Keys.CONTROL, "a")); // select all
		webElement.sendKeys(Keys.DELETE);

	}

	protected void setElementValue(WebElement webElement, String data) {
		JavascriptExecutor js = (JavascriptExecutor) (getPage().getDriver());

		js.executeScript("arguments[0].value='" + data + "';", webElement);
	}

	protected void setElementValue(WebElement webElement, int data) {
		JavascriptExecutor js = (JavascriptExecutor) (getPage().getDriver());

		js.executeScript("arguments[0].value=" + data + ";", webElement);
	}

	protected void moveTo(WebElement element) {
		assertNotNull(element);
		Actions builder = new Actions(getPage().getDriver());
		builder.moveToElement(element).build().perform();
	}

	protected void pressKeyboardEvent(String name) {
		assertNotNull(name);
		pressKeyboardEvent(KeyEventUtils.getKeyCode(name));

	}

	protected void pressKeyboardEvent(int keyEvent) {
		try {
			Robot robot = new Robot();

			robot.keyPress(keyEvent);
			getPage().waitFor(200);
			robot.keyRelease(keyEvent);
			getPage().waitFor(200);
		} catch (Exception e) {
		}

	}

	protected void pressKeyboardEventsStr(List<String> names) {
		assertNotNull(names);
		List<Integer> keyEvents = Utils.toListInt(names);
		pressKeyboardEvents(keyEvents);

	}

	protected void pressKeyboardEvents(List<Integer> keyEvents) {
		try {
			Robot robot = new Robot();
			for (int keyEvent : keyEvents) {
				robot.keyPress(keyEvent);
			}

			getPage().waitFor(200);

			for (int keyEvent : keyEvents) {
				robot.keyRelease(keyEvent);
			}
		} catch (Exception e) {
			LOGGER.error("Press keyboard error: " + e);
		}

	}

	protected void switchFrame(WebElement iframe) {
		assertNotNull(iframe);
		getPage().getDriver().switchTo().frame(iframe);
	}

	protected void switchToParentFrame() {
		getPage().getDriver().switchTo().parentFrame();
	}

	/****************************************************************************************************
	 * ASSERTION BASE STEP
	 ******************************************************************************************************/

	protected void assertTextEquals(WebElement webElement, String data) {
		assertTextEquals(data.trim(), getText(webElement).trim());
	}

	protected void assertTextEquals(String expected, String actual) {
		assertEquals(expected, actual);
		// assertTrue(String.format("Expected '%s' but actual is '%s'",
		// expected, actual), Utils.equalsIgnoringNewlineStyle(expected,
		// actual));
		// getPage().waitFor(100));
	}

	protected void assertTextEquals(List<WebElement> webElements, List<String> datas) {
		int size = webElements.size();
		assertEquals("List element should equal to list data.", size, datas.size());
		for (int i = 0; i < size; i++) {
			assertTextEquals(webElements.get(i), datas.get(i));
		}

	}

	protected void assertNumbersEqual(List<WebElement> webElements, List<Integer> datas) {
		int size = webElements.size();
		assertEquals("List element should equal to list data.", size, datas.size());
		for (int i = 0; i < size; i++) {
			Integer actual = Utils.toInt(getText(webElements.get(i)));
			Integer expected = datas.get(i);
			assertEquals(expected, actual);
		}

	}

	protected String getText(String objPath) {
		WebElement element = findElement(objPath);
		assertNotNull(objPath + " should be visible.", element);
		scrollTo(element);
		return getText(element);
	}

	protected String getText(WebElement element) {
		assertNotNull(element);
		String r = "";
		if (element.getTagName().equals("input")) {
			r = element.getAttribute("value");
		} else {
			r = element.getText();
		}

		return r;
	}

	protected void storeData(Object key, Object value) {
		AppContext.get().setCacheData(key, value);
	}

	protected Object getCacheData(Object key) {
		return AppContext.get().getCacheData(key);
	}

	/**
	 * 
	 * @param webElement
	 * @param attribute
	 *            name, value, visible, clickable
	 * @return
	 */
	protected String getAttribute(WebElement webElement, String attribute) {
		assertNotNull(webElement);
		assertNotNull(attribute);
		return webElement.getAttribute(attribute);
	}

	protected String getAttribute(String objPath, String attribute) {

		WebElement webElement = findElement(objPath);
		assertNotNull(objPath + " should be available.", webElement);
		assertNotNull(attribute);
		return webElement.getAttribute(attribute);
	}

	/****************************************************************************************************
	 * DATA INPUT/VERIFICATION BASE STEP
	 ******************************************************************************************************/

	protected boolean waitInvisibility(String objPath, int timeout) {
		boolean r = false;
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(getPage().getDriver()).withTimeout(timeout, TimeUnit.SECONDS)
				.pollingEvery(500, TimeUnit.MILLISECONDS);
		try {
			r = wait.until(ExpectedConditions.invisibilityOfElementLocated(SeleniumUtils.getBy(objPath, this.pathParams)));

		} catch (Exception e) {
			// TODO: handle exception
		}
		return r;

	}

	protected boolean waitVisibility(String objPath, int seconds) {
		boolean r = false;
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(getPage().getDriver()).withTimeout(seconds, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class);
		try {
			r = (null != wait.until(ExpectedConditions.visibilityOfElementLocated(SeleniumUtils.getBy(objPath, this.pathParams))));
		} catch (TimeoutException te) {
			r = false;
		}
		return r;

	}

	protected void click(String objPath) {
		WebElement webElement = findElement(objPath);
		click(objPath, webElement);
	}

	protected void click(String objPath, String name) {
		WebElement webElement = findElement(objPath);
		click(name, webElement);
	}

	protected void click(String objPath, int index) {
		WebElement webElement = findElementAt(objPath, index);
		click(objPath, webElement);
	}

	protected void click(String objPath, int index, String name) {
		WebElement webElement = findElementAt(objPath, index);
		click(name, webElement);
	}

	protected void clickIfExist(String objPath, int seconds) {
		if (waitVisibility(objPath, seconds)) {
			click(objPath);
		}
	}

	protected void doubleClick(String objPath) {
		WebElement webElement = findElement(objPath);
		doubleClick(objPath, webElement);
	}

	protected void clickWithoutScroll(String objPath) {
		WebElement webElement = findElement(objPath);
		clickWithoutScroll(objPath, webElement);
	}

	protected void clickJS(String objPath) {
		WebElement webElement = findElement(objPath);
		click(webElement);
	}

	protected void typeText(String objPath, String value) {
		WebElement webElement = findElement(objPath);
		sendKeys(webElement, value);

	}

	protected WebElement findElement(String objPath) {
		try {
			return findElement(SeleniumUtils.getBy(objPath, this.pathParams));
		} catch (Exception e) {
			return null;
		}

	}

	protected WebElement getContainerElement() {
		return (WebElement) AppContext.get().getCacheData(ConstUtils.CACHE_CONTAINER_ELEMENT);
	}

	protected String getContainerName() {
		String result = (String) AppContext.get().getCacheData(ConstUtils.CACHE_CONTAINER_NAME);
		if (result == null) {
			return "";
		} else {
			return result;
		}
	}

	protected List<WebElement> findElements(WebElement element, By by) {
		try {
			return element.findElements(by);
		} catch (Exception e) {
			return null;
		}
	}

	protected List<WebElement> findElements(By by) {
		WebElement container = getContainerElement();
		if (container != null) {
			return findElements(container, by);
		}
		return getPage().getDriver().findElements(by);
	}

	protected WebElement findElement(By by) {

		WebElement container = getContainerElement();
		if (container != null) {
			return findElement(container, by);
		}

		try {
			return getPage().getDriver().findElement(by);
		} catch (Exception e) {
			return null;
		}
	}

	protected WebElement findElement(WebElement container, By by) {

		try {
			return container.findElement(by);
		} catch (Exception e) {
			return null;
		}
	}

	protected List<WebElement> findElements(String objPath) {
		try {
			return findElements(SeleniumUtils.getBy(objPath, this.pathParams));
		} catch (Exception e) {
			return new ArrayList<>();
		}

	}

	protected void setPathParams(String... params) {
		this.pathParams = params;
	}

	protected boolean waitClickable(String objPath, int timeout) {
		boolean r = false;

		FluentWait<WebDriver> wait = new FluentWait(getPage().getDriver()).withTimeout(timeout, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class);
		r = wait.until(ExpectedConditions.elementToBeClickable(SeleniumUtils.getBy(objPath))) != null;
		return r;

	}

	protected void checkNotExist(String objPath) {
		WebElement element = findElement(objPath);
		assertNull(objPath + " must not be in the view.", element);

	}

	protected void checkNotExist(String objPath, String name) {
		WebElement element = findElement(objPath);
		assertNull(name + " must not be in the view.", element);

	}

	protected void checkExist(String objPath) {
		WebElement element = findElement(objPath);
		assertNotNull(objPath + " must be in the view.", element);

	}

	protected void checkExist(String objPath, String name) {
		WebElement element = findElement(objPath);
		assertNotNull(name + " must be in the view.", element);

	}

	protected void checkExist(String name, WebElement webElement) {
		assertNotNull(name + " must be in the view.", webElement);

	}

	protected boolean isExisted(String objPath) {
		WebElement element = findElement(objPath);
		if (element != null) {
			return true;
		} else {
			return false;
		}
	}

	protected boolean isNotExisted(String objPath) {
		WebElement element = findElement(objPath);
		if (element != null) {
			return false;
		} else {
			return true;
		}
	}

	protected String getRandomId() {
		Random rand = new Random(System.currentTimeMillis());
		int id = Math.abs(rand.nextInt());
		return Integer.toString(id);
	}

	protected WebElement findElementEquals(String name, List<WebElement> elements) {
		for (WebElement e : elements) {
			if (name.trim().equals(getText(e).trim())) {
				return e;
			}
		}
		return null;
	}

	protected WebElement findElementAt(List<WebElement> elements, int index) {
		if (elements.size() >= index) {
			return elements.get(index - 1);
		}
		return null;
	}

	protected WebElement findElementAt(String objPath, int index) {
		List<WebElement> elements = findElements(objPath);
		return findElementAt(elements, index);
	}

	protected void setContainer(String name, WebElement container) {
		AppContext.get().setCacheData(ConstUtils.CACHE_CONTAINER_NAME, name);
		AppContext.get().setCacheData(ConstUtils.CACHE_CONTAINER_ELEMENT, container);
		LOGGER.warn("Running in container: " + name);
	}

	protected void verifyExist(String name, WebElement webElement) {
		assertNotNull(name + " should be visible in the view", webElement);
	}

	protected void verifyNotExist(String name, WebElement webElement) {
		assertNull(name + " should not be visible in the view", webElement);
	}

	protected void verifyEqualsIgnoreCase(String value, String actual) {
		assertEquals(value.trim().toLowerCase(), actual.trim().toLowerCase());
	}

	protected void verifyEquals(String value, String actual) {
		assertEquals(value.trim(), actual.trim());
	}

	protected void verifyEqualsWithTag(String tag, String expected, String actual) {
		assertEquals(expected.trim() + tag.trim(), actual.trim());
	}

	protected void verifyStartsWith(String expected, String actual) throws AssertionError {
		if (!actual.trim().startsWith(expected.trim())) {
			throw new AssertionError(String.format("Expected starts with '%s' but actual is '%s'", expected, actual));
		}
	}

	protected void verifyEndsWith(String expected, String actual) throws AssertionError {
		if (!actual.trim().endsWith(expected.trim())) {
			throw new AssertionError(String.format("Expected ends with '%s' but actual is '%s'", expected, actual));
		}
	}

	protected void verifyContains(String expected, String actual) throws AssertionError {
		if (!actual.trim().contains(expected.trim())) {
			throw new AssertionError(String.format("Expected contains '%s' but actual is '%s'", expected, actual));
		}
	}

	protected void verifySorted(List<?> collection, String sortType) {
		boolean sorted = false;
		switch (sortType) {
		case "ASCENDING":
			sorted = Ordering.natural().isOrdered((Iterable<? extends Comparable>) collection);
			break;
		case "DESCENDING":
			sorted = Ordering.natural().reverse().isOrdered((Iterable<? extends Comparable>) collection);
			break;
		default:
			break;
		}

		if (!sorted) {
			throw new AssertionError("Collection is not sorted by " + sortType);
		}
	}

	protected void clearCachedData() {
		AppContext.get().clearCachedData();
	}

	protected void switchOutContainer() {
		AppContext.get().setCacheData(ConstUtils.CACHE_CONTAINER_ELEMENT, null);
		LOGGER.warn("Running out of container: ");
	}

	protected void switchMode(String mode) {
		AppContext.get().setAttribute("current", AppContext.get().getAttribute(mode + "Url"));
	}

	protected void switchToPlatform(String platform) {
		AppContext.get().setAttribute(ConstUtils.WEBDRIVER, platform);
		getPage().setProxiedDriver(new MyDriver().newDriver());
	}

	protected void wait(int seconds) {
		waitFor(1000 * seconds);
	}

	protected void waitFor(int millisec) {
		try {
			Thread.sleep(millisec);
		} catch (Exception e) {
		}
	}

	protected void verifyDateFormat(String dateToBeCheck, String dateFormat) {
		Date date = null;
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		try {
			date = sdf.parse(dateToBeCheck);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		assertNotNull("Date is wrong format: " + dateToBeCheck, date);
	}
}
