
package autotest.bdd.framework.steps.basic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jbehave.core.annotations.Aliases;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import autotest.bdd.framework.utils.ConstUtils;
import autotest.bdd.framework.utils.Utils;

public class ExcelBaseSteps extends BaseSteps {
	private static final Logger LOGGER = LogManager.getLogger(ExcelBaseSteps.class);
	HSSFWorkbook workbook;
	XSSFWorkbook xs_workbook;
	HSSFSheet sheetReader;
	XSSFSheet xs_sheetReader;

	private List<String> headers;
	private List<List<Object>> tableData;
	private Map<String, List<Object>> columnData;

	private int totalRow;
	private int totalColumn;

	@Then("I should see '$colName' column at row #$row is '$value' in excel file")
	@Aliases(values = { "I should see '$colName' column at row #$row is '$value' in csv file" })
	public void verifyColumnEquals(String colName, int row, String value) {
		assertTextEquals(columnData.get(colName).get(row - 1).toString(), value);
	}

	@Then("I should see column '$colName' in excel file")
	@Aliases(values = { "I should see column '$colName' in csv file" })
	public void verifyHeadersEquals(String colName) {
		columnData.containsKey(colName);
	}

	@When("I open downloaded excel file")
	public void readLatestDowloadExcelFile() {
		File path = Utils.getLatestFile(ConstUtils.DOWNLOAD_DIR, "xls");
		try {
			FileInputStream in = new FileInputStream(path);
			workbook = new HSSFWorkbook(in);
		} catch (Exception e) {
			LOGGER.error("Cannot read Excel file " + path);
			LOGGER.error(e);
		}
	}

	@When("I read sheet No. $index in excel file")
	public void readSheet(int index) {
		sheetReader = workbook.getSheetAt(index - 1);
		headers = new ArrayList<>();
		tableData = new ArrayList<>();
		columnData = new HashMap<>();

		int startRow = 0;
		Row row = null;
		do {
			row = sheetReader.getRow(startRow);
			totalColumn = row.getLastCellNum();
			if (totalColumn > 1) {
				break;
			}
			startRow++;
		} while (totalColumn <= 1);

		totalRow = sheetReader.getLastRowNum();
		System.out.println("There are " + totalRow + " records in Excel file!!!");

		// get header
		for (int j = 0; j < totalColumn; j++) {
			// headers.add(getCellValue(0, j));
			headers.add(getCellValue(startRow, j));
			columnData.put(headers.get(j), new ArrayList<>());
		}

		// get table data, column data
		readData: for (int i = (startRow + 1); i <= totalRow; i++) {
			row = sheetReader.getRow(i);
			ArrayList<Object> rowValues = new ArrayList<>();

			for (int j = 0; j < totalColumn; j++) {
				List<Object> colValues = columnData.get(headers.get(j));
				String cellValue = getCellValue(row, j);
				if (cellValue == null) {
					break readData;
				}
				cellValue = cellValue.trim();
				System.out.print(cellValue + "\t| ");
				colValues.add(cellValue);
				columnData.put(headers.get(j), colValues);

				rowValues.add(cellValue);
			}
			tableData.add(rowValues);
			System.out.print("\n");
		}

		storeData(ConstUtils.CACHED_EXCEL_HEADER, headers);
		storeData(ConstUtils.CACHED_EXCEL_TABLE_DATA, tableData);
		storeData(ConstUtils.CACHED_EXCEL_COLUMN_DATA, columnData);
	}

	// Get cell value
	public String getCellValue(int rownum, int column) {
		Row row = sheetReader.getRow(rownum);
		return getCellValue(row, column).trim();
	}

	// Get cell value and convert to string
	public String getCellValue(Row row, int column) {
		Cell cell = row.getCell(column);
		String cellValue = "";
		DataFormatter df = new DataFormatter();
		if (cell == null || cell.getCellTypeEnum() == CellType.BLANK) {
			return cellValue;
		}

		if (row != null) {
			switch (cell.getCellTypeEnum()) {
			case STRING:
				cellValue = cell.getStringCellValue();
				break;

			case FORMULA:
				cellValue = cell.getCellFormula();
				break;

			case NUMERIC:
				if (DateUtil.isCellDateFormatted(cell)) {
					cellValue = df.formatCellValue(cell);
				} else {
					cellValue = Long.toString(Math.round(cell.getNumericCellValue()));
				}
				break;

			case BLANK:
				cellValue = "";
				break;

			case BOOLEAN:
				cellValue = Boolean.toString(cell.getBooleanCellValue());
				break;
			default:
				break;

			}
		}
		return cellValue;
	}

	public List<String> getColumnNames() {
		return headers;
	}

	public List<Object> getColumnData(String colName) {
		return columnData.get(colName);
	}

	public int getTotalDataRow() {
		return totalRow;
	}

	public int getTotalColumn(int row) {
		return totalColumn;
	}

	protected void storeExcelField(String name, Object value) {
		storeData(ConstUtils.CACHED_EXCEL_FIELD_PREFIX + name, value);
	}

	@When("I open downloaded csv file and read data")
	public void readCsvFile() {
		headers = new ArrayList<>();
		tableData = new ArrayList<>();
		columnData = new HashMap<>();
		String line = "";
		BufferedReader br = null;
		try {
			// Read csv file
			File latestDownloadFile = Utils.getLatestFile(ConstUtils.DOWNLOAD_DIR, "csv");
			String absolutePath = latestDownloadFile.getAbsolutePath();
			br = new BufferedReader(new FileReader(absolutePath));

			// Get Header
			if ((line = br.readLine()) != null) {
				// special case for starpay management report
				if ("StarPay Management".equals(line)) {
					line = br.readLine();
				}
				String[] text;
				if (line.contains("\",\"")) {
					// case 1: line="No","Location","Merchant Name","Merchant
					// Email","Transaction Date","Total Voucher
					// Amount","Status",
					text = line.split("\",\"");
				} else {
					// case 2:line=No,Member Number,Profile Created
					// Date,Card,Card Added Date,Status,Remarks
					text = line.split(",");
				}
				for (int j = 0; j < text.length; j++) {
					headers.add(text[j].replace("\"", "").replace("\",", ""));
					columnData.put(headers.get(j), new ArrayList<>());
				}
			}

			// Get data
			totalRow = 0;
			while ((line = br.readLine()) != null) {
				String[] text;
				if (line.contains("\",\"") && !line.contains(",,")) {
					// case 1: line="No","Location","Merchant Name","Merchant
					// Email","Transaction Date","Total Voucher
					// Amount","Status",
					text = line.split("\",\"");
				} else {
					// case 2:line=No,Member Number,Profile Created
					// Date,Card,Card Added Date,Status,Remarks
					text = line.split(",");
				}
				ArrayList<Object> rowValues = new ArrayList<>();
				for (int j = 0; j < text.length; j++) {
					List<Object> value = columnData.get(headers.get(j));
					String dataPiece = text[j].replace("\"", "").replace("\",", "").trim();
					value.add(dataPiece);
					rowValues.add(dataPiece);
					// System.out.print(dataPiece + "\t| ");
					columnData.put(headers.get(j), value);
				}
				// System.out.print("\n");
				tableData.add(rowValues);
				totalRow++;
			}
			System.out.println("There are " + totalRow + " records in CSV file!!!");
			// Store data
			storeData(ConstUtils.CACHED_EXCEL_HEADER, headers);
			storeData(ConstUtils.CACHED_EXCEL_TABLE_DATA, tableData);
			storeData(ConstUtils.CACHED_EXCEL_COLUMN_DATA, columnData);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}