package autotest.bdd.framework.steps.basic.mobile.android;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.jbehave.core.annotations.Then;
import org.openqa.selenium.WebElement;

import autotest.bdd.framework.model.AndroidWidget;
import autotest.bdd.framework.utils.Utils;

public class AndroidVerificationSteps extends AndroidBaseActionSteps {

	/****************************************************************************************************
	 * VERIFY ANDROID ELEMENT EXIST OR NOT
	 ******************************************************************************************************/

	@Then("I should see id '$id' displays in AndroidApp")
	public void verifyElementWithIdExist(String id) {
		WebElement webElement = getElementWithId(id);
		verifyExist(id, webElement);
	}

	@Then("I should see the $type name '$text' displays in AndroidApp")
	public void verifyWidgetWithTextExist(String type, String text) {
		WebElement webElement = getWidgetWithText(type, text);
		verifyExist(type, webElement);
	}

	@Then("I should see the $type contains '$text' displays in AndroidApp")
	public void verifyWidgetWithTextContainsExist(String type, String text) {
		WebElement webElement = getWidgetWithTextContains(type, text);
		verifyExist(type, webElement);
	}

	@Then("I should see $type id '$id' displays in AndroidApp")
	public void verifyWidgetWithIdExist(String type, String id) {
		WebElement webElement = getWidgetWithId(type, id);
		verifyExist(type + " " + id, webElement);
	}

	@Then("I should not see id '$id' in AndroidApp")
	public void verifyElementWithIdNotExist(String id) {
		WebElement webElement = getElementWithId(id);
		verifyNotExist(id, webElement);
	}

	@Then("I should not see the $type name '$text' in AndroidApp")
	public void verifyWidgetWithTextNotExist(String type, String text) {
		WebElement webElement = getWidgetWithText(type, text);
		verifyNotExist(type, webElement);
	}

	@Then("I should not see $type id '$id' in AndroidApp")
	public void verifyWidgetWithIdNotExist(String type, String id) {
		WebElement webElement = getWidgetWithId(type, id);
		verifyNotExist(type + " " + id, webElement);
	}

	/****************************************************************************************************
	 * VERIFY ANDROID ELEMENT VALUE
	 ******************************************************************************************************/

	@Then("I should see id '$id' is '$value' in AndroidApp")
	public void verifyElementWithIdValue(String id, String value) {
		WebElement webElement = getElementWithId(id);
		assertEquals(value, getText(webElement));
	}

	@Then("I should see id '$id' contains '$value' in AndroidApp")
	public void verifyElementWithIdContainsValue(String id, String value) {
		WebElement webElement = getElementWithId(id);
		verifyContains(value, getText(webElement));
	}

	@Then("I should see id '$id' ends '$value' in AndroidApp")
	public void verifyElementWithIdEndsWithValue(String id, String value) {
		WebElement webElement = getElementWithId(id);
		verifyEndsWith(value, getText(webElement));
	}

	@Then("I should see the $type is '$value' in AndroidApp")
	public void verifyWidgetValue(String type, String value) {
		WebElement webElement = getWidget(type);
		assertEquals(value, getText(webElement));
	}

	@Then("I should see $type id '$id' is '$value' in AndroidApp")
	public void verifyWidgetWithIdValue(String type, String id, String value) {
		WebElement webElement = getWidgetWithId(type, id);
		assertEquals(value, getText(webElement));
	}

	@Then("I should see $type id '$id' contains '$value' in AndroidApp")
	public void verifyWidgetWithIdContainsValue(String type, String id, String value) {
		WebElement webElement = getWidgetWithId(type, id);
		verifyContains(value, getText(webElement));
	}

	@Then("I should see $type id '$id' ends '$value' in AndroidApp")
	public void verifyWidgetWithIdEndsWithValue(String type, String id, String value) {
		WebElement webElement = getWidgetWithId(type, id);
		verifyEndsWith(value, getText(webElement));
	}

	@Then("I should see $type id '$id' state is $state in AndroidApp")
	public void verifyWidgetWithIdState(String type, String id, String state) {
		WebElement webElement = getWidgetWithId(type, id);
		verifyWidgetState(AndroidWidget.getAndroidWidget(type), type + " " + id, webElement, state);
	}

	@Then("I should see $type name '$text' state is $state in AndroidApp")
	public void verifyWidgetWithTextState(String type, String text, String state) {
		WebElement webElement = getWidgetWithText(type, text);
		verifyWidgetState(AndroidWidget.getAndroidWidget(type), type + " " + text, webElement, state);
	}

	private void verifyWidgetState(AndroidWidget type, String name, WebElement webElement, String state) {

		String attribute = "selected";
		if (type.equals(AndroidWidget.Switch)) {
			attribute = "checked";
		}

		String actual = getAttribute(webElement, attribute);
		boolean actualState = Utils.getBoolValue(actual);
		if (SELECT_STATE.contains(state)) {
			assertTrue(name + " should be " + attribute, actualState);
		} else {
			assertFalse(name + " should not be " + attribute, actualState);
		}
	}

}
