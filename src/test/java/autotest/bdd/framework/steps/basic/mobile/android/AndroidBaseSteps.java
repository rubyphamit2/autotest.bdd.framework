package autotest.bdd.framework.steps.basic.mobile.android;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.jbehave.core.annotations.When;
import org.openqa.selenium.WebElement;

import autotest.bdd.framework.model.AndroidWidget;
import autotest.bdd.framework.steps.basic.mobile.MobileBaseSteps;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.android.connection.ConnectionState;

public class AndroidBaseSteps extends MobileBaseSteps {

	/**
	 * 
	 * @param type
	 */
	@When("I set connection to '$type' on mobile device")
	public void setConnection(String type) {
		AppiumDriver<?> driver = getAppiumDriver();
		if (driver instanceof AndroidDriver<?>) {
			((AndroidDriver<?>) driver).setConnection(getConnectionState(type));
		} else {
			throw new AssertionError("This action support Android device only.");
		}

		wait(1);
	}

	public ConnectionState getConnectionState(String type) {
		long bitMask = 0;
		if ("AIRPLANE".equalsIgnoreCase(type)) {
			bitMask = ConnectionState.AIRPLANE_MODE_MASK;
		} else if ("DATA".equalsIgnoreCase(type)) {
			bitMask = ConnectionState.DATA_MASK;
		} else if ("WIFI".equalsIgnoreCase(type)) {
			bitMask = ConnectionState.WIFI_MASK;
		}
		return new ConnectionState(bitMask);

	}

	public String getText(String objPath) {
		WebElement element = findElement(objPath);
		return getText(element);
	}

	public String getText(WebElement element) {
		assertNotNull("Element should be in the view.", element);
		String r = element.getText();
		if (StringUtils.isEmpty(r)) {
			r = element.getAttribute("text");
		}
		if (StringUtils.isEmpty(r)) {
			r = element.getAttribute("content-desc");
		}

		return r;
	}

	public String getChromeUrl() {
		return getText("Android.Chrome.Url");
	}

	@When("I press Back button on Android device")
	public void pressBackButton() {
		pressKeyCode(AndroidKeyCode.BACK);
	}

	@When("I press Enter button on Android device")
	public void pressEnterButton() {
		pressKeyCode(AndroidKeyCode.ENTER);
	}

	private void pressKeyCode(int androidKeyCode) {
		((AndroidDriver) getPage().getProxiedDriver()).pressKeyCode(androidKeyCode);
	}

	protected boolean isElementExistWithText(String text) {
		return (getElementWithText(text) != null);
	}

	protected boolean isElementExistWithTextContains(String text) {
		return (getElementWithTextContains(text) != null);
	}

	protected WebElement getWidgetWithDesc(String type, String text) {
		setPathParams(AndroidWidget.getAndroidWidget(type).toString(), text);
		return findElement("Android.Widget.WithDesc");
	}

	protected WebElement getWidgetWithDescContains(String type, String text) {
		setPathParams(AndroidWidget.getAndroidWidget(type).toString(), text);
		return findElement("Android.Widget.WithDescContain");
	}

	protected boolean isElementExistWithTextId(String id) {
		return (getElementWithId(id) != null);
	}

	/****************************************************************************************************
	 * ANDROID ELEMENT IS EXIST
	 ******************************************************************************************************/

	protected boolean isWidgetExist(String type) {
		return (getWidget(type) != null);
	}

	protected boolean isWidgetExist(String type, int index) {
		return (getWidget(type, index) != null);
	}

	protected boolean isWidgetExistWithText(String type, String text) {
		return (getWidgetWithText(type, text) != null);
	}

	protected boolean isWidgetExistWithTextContains(String type, String text) {
		return (getWidgetWithTextContains(type, text) != null);
	}

	protected boolean isWidgetExistWithDesc(String type, String text) {
		return (getWidgetWithDesc(type, text) != null);
	}

	protected boolean isWidgetExistWithDescContains(String type, String text) {
		return (getWidgetWithDescContains(type, text) != null);
	}

	protected boolean isWidgetExistWithTextId(String type, String id) {
		return (getWidgetWithId(type, id) != null);
	}

	protected boolean isElementExistWithDesc(String text) {
		return (getElementWithDesc(text) != null);
	}

	protected boolean isElementExistWithDescContains(String text) {
		return (getElementWithDescContains(text) != null);
	}

	/****************************************************************************************************
	 * ANDROID WIDGET GET WIDGET
	 ******************************************************************************************************/
	protected WebElement getWidget(String type) {
		setPathParams(AndroidWidget.getAndroidWidget(type).toString());
		return findElement("Android.Widget");
	}

	protected WebElement getWidget(String type, int index) {
		setPathParams(AndroidWidget.getAndroidWidget(type).toString());
		return findElementAt("Android.Widget", index);
	}

	protected WebElement getWidgetWithText(String type, String text) {
		setPathParams(AndroidWidget.getAndroidWidget(type).toString(), text);
		return findElement("Android.Widget.WithText");
	}

	protected WebElement getWidgetWithTextContains(String type, String text) {
		setPathParams(AndroidWidget.getAndroidWidget(type).toString(), text);
		return findElement("Android.Widget.WithTextContain");
	}

	protected WebElement getWidgetWithId(String type, String id) {
		setPathParams(AndroidWidget.getAndroidWidget(type).toString(), id);
		return findElement("Android.Widget.WithId");
	}

	protected WebElement getElementWithText(String text) {
		setPathParams(text);
		return findElement("Android.Element.WithText");
	}

	protected WebElement getElementWithTextContains(String text) {
		setPathParams(text);
		return findElement("Android.Element.WithTextContain");
	}

	protected WebElement getElementWithDesc(String text) {
		setPathParams(text);
		return findElement("Android.Element.WithDesc");
	}

	protected WebElement getElementWithDescContains(String text) {
		setPathParams(text);
		return findElement("Android.Element.WithDescContain");
	}

	protected WebElement getElementWithId(String id) {
		setPathParams(id);
		return findElement("Android.Element.WithId");
	}

}
