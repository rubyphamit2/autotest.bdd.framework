package autotest.bdd.framework.steps.basic.mobile.ios;

import org.openqa.selenium.WebElement;

import autotest.bdd.framework.model.IOSWidget;
import autotest.bdd.framework.steps.basic.mobile.MobileBaseSteps;

public class IOSBaseSteps extends MobileBaseSteps {

	/****************************************************************************************************
	 * ANDROID ELEMENT IS EXIST
	 ******************************************************************************************************/

	protected boolean isWidgetExist(String type) {
		return (getWidget(type) != null);
	}

	protected boolean isWidgetExist(String type, int index) {
		return (getWidget(type, index) != null);
	}

	protected boolean isWidgetExistWithText(String type, String text) {
		return (getWidgetWithText(type, text) != null);
	}

	protected boolean isWidgetExistWithTextContains(String type, String text) {
		return (getWidgetWithTextContains(type, text) != null);
	}


	protected boolean isWidgetExistWithTextId(String type, String id) {
		return (getWidgetWithId(type, id) != null);
	}

	protected boolean isElementExistWithDesc(String text) {
		return (getElementWithDesc(text) != null);
	}

	protected boolean isElementExistWithDescContains(String text) {
		return (getElementWithDescContains(text) != null);
	}
	
	/****************************************************************************************************
	 * IOS WIDGET GET WIDGET
	 ******************************************************************************************************/
	protected WebElement getWidget(String type) {
		setPathParams(IOSWidget.getIOSWidget(type).toString());
		return findElement("IOS.Widget");
	}

	protected WebElement getWidget(String type, int index) {
		setPathParams(IOSWidget.getIOSWidget(type).toString());
		return findElementAt("IOS.Widget", index);
	}

	protected WebElement getWidgetWithText(String type, String text) {
		setPathParams(IOSWidget.getIOSWidget(type).toString(), text);
		return findElement("IOS.Widget.WithText");
	}

	protected WebElement getWidgetWithTextContains(String type, String text) {
		setPathParams(IOSWidget.getIOSWidget(type).toString(), text);
		return findElement("IOS.Widget.WithTextContain");
	}
	
	protected WebElement getWidgetWithName(String type, String text) {
		setPathParams(type, text);
		return findElement("IOS.Widget.WithName");
	}

	protected WebElement getWidgetWithNameContains(String type, String text) {
		setPathParams(type, text);
		return findElement("IOS.Widget.WithNameContain");
	}
	protected WebElement getWidgetWithId(String type, String id) {
		setPathParams(IOSWidget.getIOSWidget(type).toString(), id);
		return findElement("IOS.Widget.WithId");
	}
	
	protected WebElement getWidgetWithIdAt(String type, String id, int index) {
		setPathParams(IOSWidget.getIOSWidget(type).toString(), id);
		return findElementAt("IOS.Widget.WithId",index);
	}

	protected WebElement getElementWithText(String text) {
		setPathParams(text);
		return findElement("IOS.Element.WithText");
	}

	protected WebElement getElementWithTextContains(String text) {
		setPathParams(text);
		return findElement("IOS.Element.WithTextContain");
	}

	protected WebElement getElementWithDesc(String text) {
		setPathParams(text);
		return findElement("IOS.Element.WithDesc");
	}

	protected WebElement getElementWithDescContains(String text) {
		setPathParams(text);
		return findElement("IOS.Element.WithDescContain");
	}

	protected WebElement getElementWithId(String id) {
		setPathParams(id);
		return findElement("IOS.Element.WithId");
	}

	protected WebElement getElementWithName(String text) {
		setPathParams(text);
		return findElement("IOS.Element.WithName");
	}

	protected WebElement getElementWithNameContains(String text) {
		setPathParams(text);
		return findElement("IOS.Element.WithNameContain");
	}

}
