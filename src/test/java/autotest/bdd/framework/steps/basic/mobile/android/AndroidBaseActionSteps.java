package autotest.bdd.framework.steps.basic.mobile.android;

import java.util.Arrays;
import java.util.List;

import org.jbehave.core.annotations.When;
import org.openqa.selenium.WebElement;

public class AndroidBaseActionSteps extends AndroidBaseSteps {

	protected static final List<String> SELECT_STATE = Arrays
			.asList(new String[] { "tick", "check", "select", "choose", "ticked", "checked", "selected", "choosen" });
	protected static final List<String> UN_SELECT_STATE = Arrays
			.asList(new String[] { "untick", "uncheck", "unselect", "unticked", "unchecked", "unselected" });

	protected static final List<String> ACTIVE_STATE = Arrays.asList(new String[] { "active", "open", "actived", "opened", "enabled" });
	protected static final List<String> INACTIVE_STATE = Arrays
			.asList(new String[] { "inactive", "close", "closed", "inactived", "disabled", "readonly" });
	
	@When("I wait for $seconds seconds")
	public void wait(int seconds) {
		super.wait(seconds);
	}

	/****************************************************************************************************
	 * SWIPE ANDROID WIDGET VALUE
	 ******************************************************************************************************/
	@When("I swipe $direction on the $type in AndroidApp")
	public void swipeOnWidget(String direction, String type) {
		swipe(direction, getWidget(type));
	}

	@When("I swipe $direction on $type No.$index in AndroidApp")
	public void swipeOnWidget(String direction, String type, int index) {
		swipe(direction, getWidget(type, index));
	}

	@When("I swipe $direction on $type name '$text' in AndroidApp")
	public void swipeOnWidgetWithText(String direction, String type, String text) {
		swipe(direction, getWidgetWithText(type, text));
	}

	@When("I swipe $direction on $type contains '$text' in AndroidApp")
	public void swipeOnWidgetWithTextContains(String direction, String type, String text) {
		swipe(direction, getWidgetWithTextContains(type, text));
	}

	@When("I swipe $direction on $type desc '$desc' in AndroidApp")
	public void swipeOnWidgetWithDesc(String direction, String type, String desc) {
		swipe(direction, getWidgetWithDesc(type, desc));
	}

	@When("I swipe $direction on $type desc contains '$desc' in AndroidApp")
	public void swipeOnWidgetWithDescContains(String direction, String type, String desc) {
		swipe(direction, getWidgetWithDescContains(type, desc));
	}

	@When("I swipe $direction on $type id '$id' in AndroidApp")
	public void swipeOnWidgetWithTextId(String direction, String type, String id) {
		swipe(direction, getWidgetWithId(type, id));
	}

	/****************************************************************************************************
	 * ANDROID TAP STEP
	 ******************************************************************************************************/

	@When("I tap on the $type in AndroidApp")
	public void tapOnWidget(String type) {
		click(getWidget(type));
	}

	@When("I tap on $type No.$index in AndroidApp")
	public void tapOnWidget(String type, int index) {
		click(getWidget(type, index));
	}

	@When("I tap on $type name '$text' in AndroidApp")
	public void tapOnWidgetWithText(String type, String text) {
		click(getWidgetWithText(type, text));
	}

	@When("I tap on $type name contains '$text' in AndroidApp")
	public void tapOnWidgetWithTextContains(String type, String text) {
		click(getWidgetWithTextContains(type, text));
	}

	@When("I tap on $type desc '$desc' in AndroidApp")
	public void tapOnWidgetWithDesc(String type, String desc) {
		System.out.println(getPage().getProxiedDriver().getPageSource());
		click(getWidgetWithDesc(type, desc));
	}

	@When("I tap on $type desc contains '$desc' in AndroidApp")
	public void tapOnWidgetWithDescContains(String type, String desc) {
		click(getWidgetWithDescContains(type, desc));
	}

	@When("I tap on $type id '$id' in AndroidApp")
	public void tapOnWidgetWithTextId(String type, String id) {
		click(getWidgetWithId(type, id));
	}

	/****************************************************************************************************
	 * ANDROID ELEMENTS
	 ******************************************************************************************************/

	@When("I tap on name '$text' in AndroidApp")
	public void tapOnElementWithText(String text) {
		click(getElementWithText(text));
	}

	@When("I tap on name contains '$text' in AndroidApp")
	public void tapOnElementWithTextContains(String text) {
		click(getElementWithTextContains(text));
	}

	@When("I tap on desc '$desc' in AndroidApp")
	public void tapOnElementWithDesc(String desc) {
		click(getElementWithDesc(desc));
	}

	@When("I tap on desc contains '$desc' in AndroidApp")
	public void tapOnElementWithDescContains(String desc) {
		click(getElementWithDescContains(desc));
	}

	@When("I tap on id '$id' in AndroidApp")
	public void tapOnElementWithId(String id) {
		click(getElementWithId(id));
	}

	/****************************************************************************************************
	 * ANDROID WIDGET INPUT STEP
	 ******************************************************************************************************/
	@When("I input '$data' into $type in AndroidApp")
	public void inputIntoWidget(String type, String data) {
		sendKeys(getWidget(type), data);
	}

	@When("I input '$data' into $type No.$index in AndroidApp")
	public void inputIntoWidget(String type, int index, String data) {
		sendKeys(getWidget(type, index), data);
	}

	@When("I input '$data' into $type name '$text' in AndroidApp")
	public void inputIntoWidgetWithText(String type, String text, String data) {
		sendKeys(getWidgetWithText(type, text), data);
	}

	@When("I input '$data' into $type name contains '$text' in AndroidApp")
	public void inputIntoWidgetWithTextContains(String type, String text, String data) {
		sendKeys(getWidgetWithTextContains(type, text), data);
	}

	@When("I input '$data' into $type desc '$desc' in AndroidApp")
	public void inputIntoWidgetWithDesc(String type, String desc, String data) {
		sendKeys(getWidgetWithDesc(type, desc), data);
	}

	@When("I input '$data' into $type desc contains '$desc' in AndroidApp")
	public void inputIntoWidgetWithDescContains(String type, String desc, String data) {
		sendKeys(getWidgetWithDescContains(type, desc), data);
	}

	@When("I input '$data' into $type id '$id' in AndroidApp")
	public void inputIntoWidgetWithId(String type, String id, String data) {
		sendKeys(getWidgetWithId(type, id), data);
	}

	/****************************************************************************************************
	 * ANDROID BASE TAP STEP
	 ******************************************************************************************************/

	@When("I swipe from element id $fromId to element id $toId in Android CSapp")
	public void swipeElementIdTo(String fromId, String toId) {
		WebElement fromElement = getElementWithId(fromId);
		WebElement toElement = getElementWithId(toId);
		swipe(fromElement, toElement);

	}

}