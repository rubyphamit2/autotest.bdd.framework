package autotest.bdd.framework.steps.basic.mobile.ios;

import org.jbehave.core.annotations.When;
import org.openqa.selenium.WebElement;

public class IOSBaseActionSteps extends IOSBaseSteps{
	/****************************************************************************************************
	 * SWIPE IOS WIDGET VALUE
	 ******************************************************************************************************/
	@When("I swipe $direction on the $type in IOSApp")
	public void swipeOnWidget(String direction, String type) {
		swipe(direction, getWidget(type));
	}

	@When("I swipe $direction on $type No.$index in IOSApp")
	public void swipeOnWidget(String direction, String type, int index) {
		swipe(direction, getWidget(type, index));
	}

	@When("I swipe $direction on $type name '$text' in IOSApp")
	public void swipeOnWidgetWithText(String direction, String type, String text) {
		swipe(direction, getWidgetWithText(type, text));
	}

	@When("I swipe $direction on $type contains '$text' in IOSApp")
	public void swipeOnWidgetWithTextContains(String direction, String type, String text) {
		swipe(direction, getWidgetWithTextContains(type, text));
	}

	@When("I swipe $direction on $type name '$name' in IOSApp")
	public void swipeOnWidgetWithName(String direction, String type, String name) {
		swipe(direction, getWidgetWithName(type, name));
	}

	@When("I swipe $direction on $type name contains '$name' in IOSApp")
	public void swipeOnWidgetWithNameContains(String direction, String type, String name) {
		swipe(direction, getWidgetWithNameContains(type, name));
	}

	@When("I swipe $direction on $type id '$id' in IOSApp")
	public void swipeOnWidgetWithTextId(String direction, String type, String id) {
		swipe(direction, getWidgetWithId(type, id));
	}

	/****************************************************************************************************
	 * IOS TAP STEP
	 ******************************************************************************************************/

	@When("I tap on the $type in IOSApp")
	public void tapOnWidget(String type) {
		click(getWidget(type));
	}

	@When("I tap on $type No.$index in IOSApp")
	public void tapOnWidget(String type, int index) {
		click(getWidget(type, index));
	}

	@When("I tap on $type name '$text' in IOSApp")
	public void tapOnWidgetWithText(String type, String text) {
		click(getWidgetWithText(type, text));
	}

	@When("I tap on $type name contains '$text' in IOSApp")
	public void tapOnWidgetWithTextContains(String type, String text) {
		click(getWidgetWithTextContains(type, text));
	}

	@When("I tap on $type name '$name' in IOSApp")
	public void tapOnWidgetWithName(String type, String name) {
		System.out.println(getPage().getProxiedDriver().getPageSource());
		click(getWidgetWithName(type, name));
	}

	@When("I tap on $type name contains '$name' in IOSApp")
	public void tapOnWidgetWithNameContains(String type, String name) {
		click(getWidgetWithNameContains(type, name));
	}

	@When("I tap on $type id '$id' in IOSApp")
	public void tapOnWidgetWithTextId(String type, String id) {
		click(getWidgetWithId(type, id));
	}

	/****************************************************************************************************
	 * IOS ELEMENTS
	 ******************************************************************************************************/

	@When("I tap on text '$text' in IOSApp")
	public void tapOnElementWithText(String text) {
		click(getElementWithText(text));
	}

	@When("I tap on text contains '$text' in IOSApp")
	public void tapOnElementWithTextContains(String text) {
		click(getElementWithTextContains(text));
	}

	@When("I tap on name '$name' in IOSApp")
	public void tapOnElementWithName(String name) {
		click(getElementWithName(name));
	}

	@When("I tap on name contains '$name' in IOSApp")
	public void tapOnElementWithNameContains(String name) {
		click(getElementWithNameContains(name));
	}

	@When("I tap on id '$id' in IOSApp")
	public void tapOnElementWithId(String id) {
		click(getElementWithId(id));
	}

	/****************************************************************************************************
	 * IOS WIDGET INPUT STEP
	 ******************************************************************************************************/
	@When("I input '$data' into $type in IOSApp")
	public void inputIntoWidget(String type, String data) {
		sendKeys(getWidget(type), data);
	}

	@When("I input '$data' into $type No.$index in IOSApp")
	public void inputIntoWidget(String type, int index, String data) {
		sendKeys(getWidget(type, index), data);
	}

	@When("I input '$data' into $type name '$text' in IOSApp")
	public void inputIntoWidgetWithText(String type, String text, String data) {
		sendKeys(getWidgetWithText(type, text), data);
	}

	@When("I input '$data' into $type name contains '$text' in IOSApp")
	public void inputIntoWidgetWithTextContains(String type, String text, String data) {
		sendKeys(getWidgetWithTextContains(type, text), data);
	}

	@When("I input '$data' into $type name '$name' in IOSApp")
	public void inputIntoWidgetWithName(String type, String name, String data) {
		sendKeys(getWidgetWithName(type, name), data);
	}

	@When("I input '$data' into $type name contains '$name' in IOSApp")
	public void inputIntoWidgetWithNameContains(String type, String name, String data) {
		sendKeys(getWidgetWithNameContains(type, name), data);
	}

	@When("I input '$data' into $type id '$id' in IOSApp")
	public void inputIntoWidgetWithId(String type, String id, String data) {
		sendKeys(getWidgetWithId(type, id), data);
	}

	/****************************************************************************************************
	 * IOS BASE TAP STEP
	 ******************************************************************************************************/

	@When("I swipe from element id $fromId to element id $toId in IOSApp")
	public void swipeElementIdTo(String fromId, String toId) {
		WebElement fromElement = getElementWithId(fromId);
		WebElement toElement = getElementWithId(toId);
		swipe(fromElement, toElement);

	}

}
