
package autotest.bdd.framework.steps.basic.mobile;

import static org.junit.Assert.assertNotNull;

import java.time.Duration;

import org.jbehave.core.annotations.When;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import autotest.bdd.framework.steps.basic.BaseSteps;
import autotest.bdd.framework.utils.Direction;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;

public class MobileBaseSteps extends BaseSteps {
	public AppiumDriver<?> getAppiumDriver() {
		return (AppiumDriver<?>) getPage().getProxiedDriver();
	}

	@When("I hide the keyboard in mobile app")
	public void hideKeyboard() {
		try {
			getAppiumDriver().hideKeyboard();

		} catch (Exception e) {
		}
	}

	public void typeTextHideKeyboard(String objPath, String value) {
		super.typeText(objPath, value);
		hideKeyboard();
	}

	@Override
	public void click(String eleName, WebElement button) {
		assertNotNull(eleName + " should be in the view.", button);

		// waitClickable(button, 20);
		button.click();
		waitFor(500);
	}

	public void waitClickable(WebElement webElement, int seconds) {
		new WebDriverWait(getAppiumDriver(), 10).until(ExpectedConditions.elementToBeClickable(webElement));
		waitFor(500);
	}

	@Override
	public void clear(WebElement webElement) {
		assertNotNull("Textfield should be in the view.", webElement);
		webElement.clear();
	}

	/**
	 * 
	 * @param element
	 *            element
	 * @param targetElement
	 *            target element
	 * @param duration
	 *            duration of the wait action. Minimum time reolution unit is
	 *            one millisecond.
	 */
	// public void swipe(WebElement element, WebElement targetElement, int
	// duration) {
	// TouchAction action = new TouchAction((AppiumDriver<WebElement>)
	// getPage().getProxiedDriver());
	// action.press(PointOption.point(xOffset, yOffset))
	//
	// .press(element)
	// .waitAction(Duration.ofMillis(duration)).moveTo(targetElement).release().perform();
	// ((AppiumDriver<WebElement>)
	// getPage().getProxiedDriver()).performTouchAction(action);
	// }

	/**
	 * 
	 * @param element
	 *            eleemnt
	 * @param targetElement
	 *            target element
	 */
	// public void swipe(WebElement element, WebElement targetElement) {
	// swipe(element, targetElement, 5000);
	// }

	/**
	 * 
	 * @param direction
	 *            direction
	 * @param objPath
	 *            String of object path defined in object repository file for
	 *            the target element.
	 * @param swipeTimes
	 *            number of swipe time
	 * @return true/false
	 */
	public boolean swipe(Direction direction, String objPath, int swipeTimes) {
		boolean r = false;
		for (int i = 0; i < swipeTimes; i++) {
			r = r & swipe(direction, objPath);
		}
		return r;
	}

	/**
	 * 
	 * @param direction
	 *            direction
	 * @param objPath
	 *            String of object path defined in object repository file for
	 *            the target element.
	 * @return true/false
	 */
	public boolean swipe(Direction direction, String objPath) {
		WebElement element = findElement(objPath);
		if (element == null) {
			LOGGER.info("Element [" + objPath + "] does not exist.");
			return false;
		}

		return swipe(direction, element);
	}

	public void swipe(String direction, WebElement element) {
		Direction _direction = Direction.getDirection(direction);
		swipe(_direction, element);

	}

	/**
	 * 
	 * @param direction
	 *            direction
	 * @param element
	 *            element
	 * @return true/false
	 */
	public boolean swipe(Direction direction, WebElement element) {
		boolean r = false;
		int x = element.getLocation().getX();
		int y = element.getLocation().getY();

		int height = element.getSize().getHeight();
		int width = element.getSize().getWidth();

		switch (direction) {
		case UP:
			swipe(x + width / 2, y + 1, x + width / 2, y + height - 1);
			break;
		case DOWN:
			swipe(x + width / 2, y + height - 1, x + width / 2, y + 1);
			break;
		case RIGHT:
			swipe(x + width - 1, y + height / 2, x + 1, y + height / 2);
			break;
		case LEFT:
			swipe(x + 1, y + height / 2, x + width - 1, y + height / 2);
			break;

		default:
			break;
		}
		r = true;

		return r;
	}

	/**
	 * Swipe a whole screen by a Direction
	 * 
	 * @param direction
	 *            direction
	 * @return true/false
	 */
	public boolean swipe(Direction direction) {
		boolean r = false;
		try {
			Dimension size = getWindowSize();
			int x = size.width / 2;
			int y = size.height / 2;

			switch (direction) {
			case UP:
				swipe(x, 1, x, size.height - 1);
				break;
			case DOWN:
				swipe(x, size.height - 1, x, 1);
				break;
			case RIGHT:
				swipe(size.width - 1, y, 1, y);
				break;
			case LEFT:
				swipe(1, y, size.width - 1, y);
				break;

			default:
				break;
			}
			r = true;

		} catch (Exception e) {

		}
		return r;
	}

	public void swipe(WebElement fromElement, WebElement toElement) {
		assertNotNull("Despature should not be null", fromElement);
		assertNotNull("Arrival should not be null", toElement);
		int startx = fromElement.getLocation().x + 1;
		int starty = fromElement.getLocation().y + 1;
		int endx = toElement.getLocation().x + 1;
		int endy = toElement.getLocation().y + 1;
		swipe(startx, starty, endx, endy);
	}

	public void swipe(int startx, int starty, int endx, int endy) {
		swipe(startx, starty, endx, endy, 3000);
	}

	/**
	 * 
	 * @param startx
	 *            startx coordinate
	 * @param starty
	 *            starty coordinate
	 * @param endx
	 *            endx coordinate
	 * @param endy
	 *            end y coordinate
	 *
	 * @param duration
	 *            Duration of the wait action. Minimum time resolution unit is
	 *            one millisecond.
	 */
	public void swipe(int startx, int starty, int endx, int endy, int duration) {
		LOGGER.info("Swiping: From (" + startx + "," + starty + ") to (" + endx + "," + endy + ") in " + duration + "ms.");
		TouchAction action = new TouchAction((AppiumDriver<WebElement>) getPage().getProxiedDriver());
		action.press(PointOption.point(startx, starty)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration)))
				.moveTo(PointOption.point(endx, endy)).release().perform();

	}

	public void tap(int x, int y) {
		TouchAction action = new TouchAction((AppiumDriver<WebElement>) getPage().getProxiedDriver());
		action.press(PointOption.point(x, y)).release().perform();
	}

	/**
	 * 
	 * @param direction
	 *            direction
	 * @param objPath
	 *            String of object path defined in object repository file for
	 *            the target element.
	 * @param swipeTimes
	 *            number of swipe time
	 * @return true/false
	 */
	public boolean swipeFastter(Direction direction, String objPath, int swipeTimes) {
		boolean r = false;
		for (int i = 0; i < swipeTimes; i++) {
			r = r & swipeFastter(direction, objPath);
		}
		return r;
	}

	/**
	 * 
	 * @param direction
	 *            direction
	 * @param objPath
	 *            String of object path defined in object repository file for
	 *            the target element.
	 * @return true/false
	 */
	public boolean swipeFastter(Direction direction, String objPath) {
		WebElement element = findElement(objPath);
		if (element == null) {
			LOGGER.info("Element [" + objPath + "] does not exist.");
			return false;
		}

		return swipeFastter(direction, element);
	}

	/**
	 * 
	 * @param direction
	 *            direction
	 * @param element
	 *            element
	 * @return true/false
	 */
	public boolean swipeFastter(Direction direction, WebElement element) {
		boolean r = false;
		int x = element.getLocation().getX();
		int y = element.getLocation().getY();

		int height = element.getSize().getHeight();
		int width = element.getSize().getWidth();

		switch (direction) {
		case UP:
			swipeFastter(x + width / 2, y + 1, x + width / 2, y + height - 1);
			break;
		case DOWN:
			swipeFastter(x + width / 2, y + height - 1, x + width / 2, y + 1);
			break;
		case RIGHT:
			swipeFastter(x + width - 1, y + height / 2, x + 1, y + height / 2);
			break;
		case LEFT:
			swipeFastter(x + 1, y + height / 2, x + width - 1, y + height / 2);
			break;

		default:
			break;
		}
		r = true;

		return r;
	}

	/**
	 * 
	 * @param startx
	 *            startx coordinate
	 * @param starty
	 *            starty coordinate
	 * @param endx
	 *            endx coordinate
	 * @param endy
	 *            end y coordinate
	 */
	public void swipeFastter(int startx, int starty, int endx, int endy) {
		swipe(startx, starty, endx, endy, 500);
	}

	/**
	 * Get size of the screen
	 * 
	 * @return Dimension
	 */
	public Dimension getWindowSize() {
		return getPage().getProxiedDriver().manage().window().getSize();
	}

	protected void reLaunchApp() {
		getPage().resetProxiedDriver();
	}
}
