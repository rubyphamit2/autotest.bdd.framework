
package autotest.bdd.framework.steps.basic;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import autotest.bdd.framework.utils.ConstUtils;
import autotest.bdd.framework.utils.Utils;

public class PDFBaseSteps extends BaseSteps {
	private static final Logger LOGGER = LogManager.getLogger(PDFBaseSteps.class);
	private String lines[];

	@When("I open downloaded pdf file")
	public void readLatestDowloadedPDFFile() throws IOException {
		File path = null;
		try {
			path = Utils.getLatestFile(ConstUtils.DOWNLOAD_DIR, "pdf");
		} catch (Exception e) {
			LOGGER.error("Cannot read PDF file " + path);
			LOGGER.error(e);
		}
		try (PDDocument document = PDDocument.load(path)) {
			document.getClass();
			if (!document.isEncrypted()) {
				PDFTextStripperByArea stripper = new PDFTextStripperByArea();
				stripper.setSortByPosition(true);
				PDFTextStripper tStripper = new PDFTextStripper();
				String pdfFileInText = tStripper.getText(document);
				// System.out.println("Text:" + st);
				// split by whitespace
				lines = pdfFileInText.split("\\r?\\n");
				for (String line : lines) {
					System.out.println(line);
				}
			}
		}
	}

	@Then("I should see pdf file contains the text '$text'")
	public void verifyContainsText(String text) {
		boolean found = false;
		for (String line : lines) {
			if (line.contains(text)) {
				found = true;
				break;
			}
		}
		assertTrue(found);
	}
}
