
package autotest.bdd.framework.steps.basic;

import org.jbehave.core.annotations.When;

import autotest.bdd.framework.api.APIService;
import autotest.bdd.framework.api.HTTPMethod;
import autotest.bdd.framework.api.model.Webservice;
import autotest.bdd.framework.utils.AppContext;
import autotest.bdd.framework.utils.ConstUtils;

public class APIBaseSteps extends BaseSteps {

	private static final APIService apiService = new APIService();

	@When("I switch to API No. $index")
	public void switchAPI(int index) {
		AppContext.get().setAttribute(ConstUtils.API_INDEX, index - 1);

	}

	private void callAPI(String method, String path, String params) {
		Webservice webservice = AppContext.get().getWebservice();
		apiService.requestWebServiceREST(webservice.getHost(), path, params, webservice.getHeaders().toString(), null,
				webservice.getUsername(), webservice.getPassword(), null, webservice.getMediaType(), method);
	}

	@When("I call GET method at path '$path' and params '$params'")
	public void webServiceGET(String path, String params) {
		callAPI(HTTPMethod.GET, path, params);
	}

	@When("I call POST method at path '$path' and params '$params'")
	public void webServicePOST(String path, String params) {
		callAPI(HTTPMethod.POST, path, params);
	}

	@When("I call PUT method at path '$path' and params '$params'")
	public void webServicePUT(String path, String params) {
		callAPI(HTTPMethod.PUT, path, params);
	}

	@When("I call DELETE method at path '$path' and params '$params'")
	public void webServiceDELETE(String path, String params) {
		callAPI(HTTPMethod.DELETE, path, params);
	}
}
