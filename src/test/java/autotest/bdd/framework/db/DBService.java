
package autotest.bdd.framework.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

import autotest.bdd.framework.utils.AppContext;

public class DBService {
	private static final Logger LOGGER = LogManager.getLogger(DBService.class);

	private static DBService instance = null;
	private DataSource datasource = null;

	private DBService() {
		createDataSource(AppContext.get().getDBConfig());
	}

	private void createDataSource(DataBase dbConfig) {
		createDataSource(dbConfig.getDriver(), dbConfig.getUrl(), dbConfig.getUsername(), dbConfig.getPassword());

	}

	public static DBService getInstance() {
		if (instance == null) {
			instance = new DBService();
		}
		return instance;
	}

	public DBService(String driverName, String dataBaseUrl, String username, String pwd) {
		createDataSource(driverName, dataBaseUrl, username, pwd);
	}

	public void createDataSource(String driverName, String dataBaseUrl, String username, String pwd) {
		PoolProperties p = new PoolProperties();
		p.setDriverClassName(driverName);
		p.setUrl(dataBaseUrl);
		p.setUsername(username);
		p.setPassword(pwd);
		p.setMaxWait(10000);
		p.setMaxActive(10);
		p.setRemoveAbandonedTimeout(60);
		p.setMinEvictableIdleTimeMillis(30000);
		p.setLogAbandoned(true);
		p.setRemoveAbandoned(true);
		p.setJdbcInterceptors(
				"org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;" + "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
		datasource = new DataSource();
		datasource.setPoolProperties(p);
	}

	public static void cleanUp() {
		if (instance != null) {
			instance.disconnect();
			instance = null;
		}
	}

	public void disconnect() {
		if (datasource != null) {
			datasource.close();
		}
	}

	public PreparedStatement createPreparedStatement(String sql) throws Exception {
		return datasource.getConnection().prepareStatement(sql);
	}

	public ResultSet executeQuery(String sql) throws Exception {
		PreparedStatement pstmt = createPreparedStatement(sql);
		return pstmt.executeQuery();
	}

	public boolean executeNonQuery(String sql) throws Exception {
		PreparedStatement pstmt = createPreparedStatement(sql);
		return pstmt.execute();
	}
}
