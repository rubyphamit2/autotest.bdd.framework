
package autotest.bdd.framework.db;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "database" })
@XmlRootElement(name = "databases")
public class DBConfigs {
	private List<DataBase> database;

	public DBConfigs() {
		super();
	}

	public List<DataBase> getDatabase() {
		if (database == null) {
			database = new ArrayList<>();
		}
		return database;
	}

	public void setDatabase(List<DataBase> database) {
		this.database = database;
	}

}
