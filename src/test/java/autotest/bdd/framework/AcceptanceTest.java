package autotest.bdd.framework;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jbehave.core.io.CodeLocations;
import org.jbehave.core.io.StoryFinder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.openqa.selenium.WebDriver;

import autotest.bdd.framework.base.InjectableSerenityStepFactory;
import net.serenitybdd.jbehave.SerenityStories;
import net.thucydides.core.webdriver.ThucydidesWebDriverSupport;

public class AcceptanceTest extends SerenityStories {
	private static final Logger LOGGER = LogManager.getLogger(AcceptanceTest.class);

	public AcceptanceTest() {
		super();
		findStoriesIn(baseFolder());
		findStoriesCalled(storyName());
	}

	protected String storyName() {
		return "*";
	}

	protected String baseFolder() {
		return "stories";
	}

	public static WebDriver getWebDriver() {
		return ThucydidesWebDriverSupport.getDriver();
	}

	@Override
	public InjectableStepsFactory stepsFactory() {
		return new InjectableSerenityStepFactory(configuration(), this.getClass().getPackage().getName(),
				getClassLoader());
	}

	@Override
	public List<String> storyPaths() {

		LOGGER.debug("Start loading stories ...");
		List<String> storiesToRun = new ArrayList<>();
		String storyProperty = System.getProperty("story");

		if (storyProperty == null || storyProperty.isEmpty()) {
			storiesToRun = super.storyPaths();
		} else {

			String[] storyNames = storyProperty.split(",");
			StoryFinder sf = new StoryFinder();
			URL baseUrl = CodeLocations.codeLocationFromClass(this.getClass());

			for (String storyName : storyNames) {
				storiesToRun.addAll(sf.findPaths(baseUrl, storyName, ""));
			}
		}
		for (String storyName : storiesToRun) {
			LOGGER.info("Found story: " + storyName);
		}

		return storiesToRun;
	}

}
