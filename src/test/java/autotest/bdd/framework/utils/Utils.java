package autotest.bdd.framework.utils;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.xml.bind.JAXBContext;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.openqa.selenium.Point;

import com.jayway.jsonpath.JsonPath;

import autotest.bdd.framework.api.model.Webservice;
import autotest.bdd.framework.api.model.Webservices;
import autotest.bdd.framework.db.DBConfigs;
import autotest.bdd.framework.db.DataBase;
import autotest.bdd.framework.selenium.SeleniumUtils;

public class Utils {
	private static final Logger LOGGER = LogManager.getLogger(Utils.class);
	private static final List<String> _TRUE = Arrays.asList(new String[] { "t", "true", "yes", "1", "(t)", "(yes)", "(true)", "(1)" });

	public static void main(String[] args) {
		System.out.println(isResourceFileExist("dbconfig.xml"));
		System.out.println(isResourceFileExist("newdbconfig.xml"));
	}

	public static long toLong(String strNo) {
		long result = -1;
		try {
			result = Long.parseLong(strNo);
		} catch (Exception e) {
			LOGGER.info(e.getMessage());
		}
		return result;
	}

	public static int toInt(String strNo) {
		int result = -1;
		try {
			result = Integer.parseInt(strNo.trim());
		} catch (Exception e) {
			LOGGER.info(e.getMessage());
		}
		return result;
	}

	public static boolean toBool(String strBool) {
		boolean result = false;
		try {
			result = Boolean.parseBoolean(strBool);
		} catch (Exception e) {
			// LOGGER.info(e.getMessage());
		}
		return result;
	}

	public static boolean getBoolValue(String strBool) {

		if (strBool == null) {
			return false;
		}
		strBool = strBool.trim();
		boolean not = false;
		if (strBool.startsWith("not")) {
			not = true;
			strBool = strBool.replace("not", "");
		}
		// TODO: handle AND, OR, XOR, NOT, ...
		boolean result = _TRUE.contains(strBool.trim().toLowerCase());

		if (not) {
			return !result;
		} else {
			return result;
		}

	}

	public static String toStr(int iNo) {
		String result = null;
		try {
			result = String.valueOf(iNo);
		} catch (Exception e) {
			LOGGER.info(e.getMessage());
		}
		return result;
	}

	public static String[] toArray(List<String> list) {
		String[] array = new String[list.size()];
		try {
			array = list.toArray(array);
		} catch (Exception e) {
			LOGGER.info(e.getMessage());
		}
		return array;
	}

	public static List<String> toListString(String[] arrayString) {
		List<String> listString = null;
		try {
			listString = Arrays.asList(arrayString);
		} catch (Exception e) {
			LOGGER.info(e.getMessage());
		}
		return listString;
	}

	public static List<Integer> toListInt(List<String> listString) {
		List<Integer> listNumbers = new ArrayList<Integer>();
		for (String string : listString) {
			listNumbers.add(toInt(string));
		}
		return listNumbers;
	}

	public static boolean isFileExist(String fileName) {
		Boolean r = false;
		try {
			File f = new File(fileName);
			if (f.exists() && !f.isDirectory()) {
				r = true;
			}
		} catch (Exception e) {
			LOGGER.info(e.getMessage());
		}
		return r;
	}

	public static boolean isResourceFileExist(String fileName) {
		URL url = null;
		try {
			url = Thread.currentThread().getContextClassLoader().getResource(fileName);

		} catch (Exception e) {
			LOGGER.info(e.getMessage());
		}
		if (url != null) {
			return true;
		} else {
			return false;
		}
	}

	public static Boolean renameFile(String existFile, String newFile) {
		Boolean r = false;
		try {
			r = new File(existFile).renameTo(new File(newFile));
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
		return r;
	}

	public static ArrayList<String> getFilesInFolder(String folderPath) {
		ArrayList<String> r = new ArrayList<String>();
		File folder = new File(folderPath);
		for (File fileEntry : folder.listFiles()) {
			if (!fileEntry.isFile())
				continue;
			r.add(fileEntry.getPath());
		}
		return r;
	}

	public static Properties getProperties(File dir) {
		Properties propObjRepo = new Properties();
		if (dir.isDirectory()) {
			File[] files = dir.listFiles();
			for (File file : files) {
				propObjRepo.putAll(getProperties(file));
			}
		} else {
			try {
				propObjRepo.load(new FileInputStream(dir));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return propObjRepo;
	}

	public static ArrayList<String> getFileNames(String folderPath) {
		ArrayList<String> r = new ArrayList<String>();
		File folder = new File(folderPath);
		for (File fileEntry : folder.listFiles()) {
			if (!fileEntry.isFile())
				continue;
			r.add(fileEntry.getName());
		}
		return r;
	}

	public static DateTime parseStringToDate(String date, String formatDate) {
		DateTimeFormatter formatter = DateTimeFormat.forPattern(formatDate);
		return formatter.parseDateTime(date);

	}

	public static <E> void addAll(Collection<E> collection, Collection<E> collectionTemp, boolean removeDuplicate) {
		for (E e : collectionTemp) {
			if (!removeDuplicate || (removeDuplicate && !collection.contains(e))) {
				collection.add(e);
			}
		}
	}

	public static boolean isWithinBoundary(int boundary1, int boundary2, int num, boolean includeLeftEdge, boolean includeRightEdge) {
		if (includeLeftEdge && num >= boundary1 && includeRightEdge && boundary2 >= num) {
			return true;
		} else if (!includeLeftEdge && num > boundary1 && includeRightEdge && boundary2 >= num) {
			return true;
		} else if (includeLeftEdge && num >= boundary1 && !includeRightEdge && boundary2 > num) {
			return true;
		} else if (!includeLeftEdge && num > boundary1 && !includeRightEdge && boundary2 > num) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isWithinBoundary(DateTime boundary1, DateTime boundary2, int num, TimeUnit timeUnit, boolean includeEdge) {
		boolean result = false;

		switch (timeUnit) {
		case DAYS:
			boundary1 = boundary1.plusDays(num);
			result = boundary1.isBefore(boundary2) || boundary1.isEqual(boundary2);
			break;

		default:
			break;
		}

		return result;
	}

	public static boolean isWithinBoundary(int boundary1, int boundary2, int num1, int num2, boolean includeLeftEdge,
			boolean includeRightEdge) {
		return isWithinBoundary(boundary1, boundary2, num1, includeLeftEdge, includeRightEdge)
				&& isWithinBoundary(boundary1, boundary2, num2, includeLeftEdge, includeRightEdge);
	}

	/**
	 * @param list
	 * @param name
	 * @return number of element with name on list
	 */
	public static int countElements(List<String> list, String name) {
		List<String> listElements = new ArrayList<String>();
		listElements.addAll(list);
		int count = 0;
		boolean stop = false;
		while (!stop) {
			if (listElements.contains(name)) {
				listElements.remove(name);
				count++;
			} else {
				stop = true;
			}
		}
		return count;
	}

	/**
	 * Compare 2 numbers
	 * 
	 * @param firstNum
	 * @param secondNum
	 * @return
	 */
	public static int compare(int firstNum, int secondNum) {
		if (firstNum == secondNum) {
			return 0;
		} else if (firstNum > secondNum) {
			return 1;
		} else {
			return -1;
		}
	}

	public static boolean isTrue(Boolean value) {
		return value != null && value;
	}

	public static boolean isFalse(Boolean value) {
		return value != null && !value;
	}

	public static boolean isTrue(String value) {
		boolean r = false;
		try {
			r = Boolean.getBoolean(value);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return r;
	}

	public static boolean isFalse(String value) {
		boolean r = false;
		try {
			r = !Boolean.getBoolean(value);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return r;
	}

	public static void sort(SortType sortType, List list) {
		switch (sortType) {
		case DESCENDING:
			Collections.reverse(list);
			break;
		case ASCENDING:
		default:
			Collections.sort(list);
			break;
		}

	}

	/**
	 * Time in format MM:SS i.e 00:30
	 * 
	 * @param text
	 * @return
	 */
	public static int timeInSecond(String text) {
		text = text.replace(":", "");
		return toInt(text);
	}

	public static boolean isSameLocation(Point location1, Point location2) {
		return location1.x == location2.x && location1.y == location2.y;
	}

	public static String getCurrentDateFull() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");

		return sdf.format(new Date());
	}

	/**
	 * 08/06/18 09:40 AM
	 * 
	 * @return
	 */
	public static String getLastUpdateTime() {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy HH:mm a");

		return sdf.format(new Date());
	}

	public static boolean equalsIgnoringNewlineStyle(String a, String b) {
		final char LF = '\n';
		final char CR = '\r';

		if (a == b) { // if both null return true
			return true;
		}

		if (a == null || b == null) {
			return false;
		}

		// toCharArray is slow (creates new copy of the whole string)
		// we'll use indexing instead (it's faster)

		// cleaner variable declaration (does not affect performance)
		int index_a = 0;
		int index_b = 0;

		// while (true) are a bad practice, moved loop condition to right place
		while (index_a < a.length() && index_b < b.length()) {
			char first = a.charAt(index_a);
			char second = b.charAt(index_b);

			if (first == second) {
				// decrease amount of identations
				++index_a;
				++index_b;
				continue;
			}

			if ((first != LF && first != CR) || (second != LF && second != CR)) {
				// at least one of the characters is not a new line
				return false;
			}

			if (index_a + 1 < a.length()) {
				char other = a.charAt(index_a + 1);

				// 'first' here is either \n or \r (checked before)
				// other != first ::= not { \n\n , \r\r }
				if (other != first && (other == LF || other == CR)) {
					++index_a;
				}
			}

			if (index_b + 1 < b.length()) {
				char other = b.charAt(index_b + 1);

				// same as above, but for 'second'
				if (other != second && (other == LF || other == CR)) {
					++index_b;
				}
			}

			++index_a;
			++index_b;
		}

		return index_a == a.length() && index_b == b.length();
	}

	/**
	 * 
	 * @param type
	 *            Calendar.DATE; Calendar.MONTH; Calendar.YEAR
	 * @param numbers
	 * 
	 * @return
	 */
	public static String getDateTime(int type, int numbers) {

		return getDateTime(type, numbers, "dd-MMM-yyyy HH:mm:ss");

	}

	public static String getDate_ddMMMyyyy(int type, int numbers) {

		return getDateTime(type, numbers, "dd-MMM-yyyy");

	}

	/**
	 * @param type
	 * @param numbers
	 * @param format
	 * @return
	 */
	public static String getDateTime(int type, int numbers, String format) {
		SimpleDateFormat formatter = new SimpleDateFormat(format);

		Calendar cal = Calendar.getInstance();
		cal.add(type, numbers);

		return (formatter.format(cal.getTime()));
	}

	public static List<DataBase> getDbConfigs(String fileName) {
		List<DataBase> database = new ArrayList<>();
		try {
			JAXBContext context = JAXBContext.newInstance(DBConfigs.class);
			InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
			;
			StringReader reader = new StringReader(IOUtils.toString(is));
			DBConfigs dbConfigs = (DBConfigs) context.createUnmarshaller().unmarshal(reader);
			for (DataBase db : dbConfigs.getDatabase()) {
				db.setUrl(db.getUrl().replaceAll("(\\\\)\\\\", "$1"));
			}
			database = dbConfigs.getDatabase();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return database;
	}

	public List<Object> extractResponse(String response, String format, String... jsonPaths) {
		List<Object> result = new ArrayList<Object>();
		if (response == null) {
			return result;
		}
		switch (format) {
		case "JSON":
			Object document = parseJsonToDoc(response);
			for (String jPath : jsonPaths) {
				String jsonPath = SeleniumUtils.getObjPath(jPath);
				Object actual;
				if (StringUtils.isNotEmpty(jsonPath)) {
					actual = readJsonObject(document, jsonPath);
				} else {
					actual = readJsonObject(document, jPath);
				}
				if (actual != null) {
					result.add(actual);
				} else {
					result.add("");
				}
			}
			break;

		default:
			break;
		}

		return result;
	}

	public static Object parseJsonToDoc(String response) {
		LOGGER.debug("Parsing json document:\n{}\n", response);
		return com.jayway.jsonpath.Configuration.defaultConfiguration().jsonProvider().parse(response);
	}

	public static Object readJsonObject(Object document, String jsonPath) {
		try {
			return JsonPath.read(document, jsonPath);
		} catch (Exception e) {
			return null;
		}
	}

	public static List<Webservice> getWebservices(String fileName) {
		List<Webservice> webservices = new ArrayList<>();
		try {
			JAXBContext context = JAXBContext.newInstance(Webservices.class);
			InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
			;
			StringReader reader = new StringReader(IOUtils.toString(is));
			Webservices _webservices = (Webservices) context.createUnmarshaller().unmarshal(reader);
			// for (Webservice db : dbConfigs.getWebservice()) {
			// db.setUrl(db.getUrl().replaceAll("(\\\\)\\\\", "$1"));
			// }
			webservices = _webservices.getWebservice();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return webservices;
	}

	public static Date converDateTime(String date, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);

		try {
			return sdf.parse(date);
		} catch (ParseException e) {
			return null;
		}
	}

	public static String convertDateTime(Date date, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);

		return sdf.format(date);
	}

	/**
	 * 
	 * @param inputFieldData
	 *            Ascott ( SIN100 ) --> Ascott
	 * @return
	 */
	public static String removeInsindeBrackets(String inputFieldData) {
		return inputFieldData.replaceAll("\\(.*?\\)", "");
	}

	public static File getLatestFile(String filePath, String ext) {
		File theNewestFile = null;
		File dir = new File(filePath);
		if (dir == null || !dir.isDirectory()) {
			return theNewestFile;
		}
		FileFilter fileFilter = new WildcardFileFilter("*." + ext);
		File[] files = dir.listFiles(fileFilter);

		if (files != null && files.length > 0) {
			/** The newest file comes first **/
			Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
			theNewestFile = files[0];
		}

		return theNewestFile;
	}

}
