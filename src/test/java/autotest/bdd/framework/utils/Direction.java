
package autotest.bdd.framework.utils;

public enum Direction {
	DOWN, UP, LEFT, RIGHT;

	public static Direction getDirection(String name) {
		for (Direction d : Direction.values()) {
			if (d.toString().equalsIgnoreCase(name)) {
				return d;
			}
		}
		return null;
	}
}
