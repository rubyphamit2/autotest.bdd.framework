
package autotest.bdd.framework.utils;

import java.io.File;

public class ConstUtils {

	public static final String BASE_DIR = System.getProperty("user.dir");
	private static final String USER_DIR = System.getProperty("user.home");
	public static final String DOWNLOAD_DIR = USER_DIR + File.separator + "downloads";

	public static final String UPLOAD_FOLDER = "upload";

	public static final String DEFAULT_SERENITY_PROPERTIES = "serenity.properties";

	public static final Object DRIVER = "driver";
	public static final String PLATFORM = "platform";
	public static final String DB_INDEX = "dbIndex";
	public static final String API_INDEX = "apiIndex";

	public static final String WEBDRIVER = "web.driver";

	public static final String OS = System.getProperty("os.name").toLowerCase();
	public static final String ANDROID = "android";
	public static final String IOS = "ios";

	public static final boolean isMac = OS.contains("mac");
	public static final boolean isWindows = OS.contains("win");

	public static final String CACHE_CONTAINER_ELEMENT = "app.container.element";
	public static final String CACHE_CONTAINER_NAME = "app.container.name";

	public static final String CACHED_API_RESULT_ENTITY = "api.entity";
	public static final String CACHED_API_RESULT_JSON_DOCUMENT = "api.jsondoc";
	public static final String CACHED_API_RESULT_HTTP_CODE = "api.httpcode";

	public static final String CACHED_DB_RESULT = "db.result";

	public static final String CACHED_EXCEL_HEADER = "excel.header";
	public static final String CACHED_EXCEL_TABLE_DATA = "excel.table.data";
	public static final String CACHED_EXCEL_COLUMN_DATA = "excel.column.data";

	public static final String CACHED_API_FIELD_PREFIX = "cache.api.Field: ";
	public static final String CACHED_DB_FIELD_PREFIX = "cache.db.Field: ";
	public static final String CACHED_EXCEL_FIELD_PREFIX = "cache.excel.Field: ";

}
