
package autotest.bdd.framework.utils;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import autotest.bdd.framework.api.model.Webservice;
import autotest.bdd.framework.base.EnvironmentProperties;
import autotest.bdd.framework.db.DataBase;

public class AppContext {

	private static final Logger LOGGER = LogManager.getLogger(AppContext.class);

	public static final ThreadLocal<AppContext> LOCALTHREAD = new ThreadLocal<AppContext>();

	private Map<Object, Object> attributes = new HashMap<>();
	private Map<Object, Object> cachedData = new HashMap<>();

	private AppContext() {

	}

	/**
	 * @param context
	 *            AppContext
	 */
	public static void set(AppContext context) {
		LOCALTHREAD.set(context);
	}

	/**
	 * @return AppContext
	 */
	public static AppContext get() {
		AppContext context = LOCALTHREAD.get();
		if (context == null) {
			context = new AppContext();
			context.setDB(0);
			context.setWebservice(0);
			context.setBaseAttributes();
			set(context);

		} else {
		}
		return context;
	}

	/**
	 * Remove object from thread local.
	 */
	public static void remove() {
		LOCALTHREAD.remove();
	}

	public void clearCachedData() {
		cachedData.clear();
	}

	public Object getCacheData(Object name) {
		Object obj = cachedData.get(name);
		return obj;
	}

	public void setCacheData(Object name, Object obj) {
		cachedData.put(name, obj);
		LOGGER.debug("Saving into CACHE: name={}, value={}", name, obj);

	}

	public Object getAttribute(Object name) {
		Object obj = attributes.get(name);
		return obj;
	}

	public void setAttribute(Object name, Object obj) {
		attributes.put(name, obj);

	}

	public Map<Object, Object> getAttributes() {
		return this.attributes;
	}

	public String getCurrentUrl() {
		String currentUrl = (String) this.getAttribute("current");
		if (currentUrl == null) {
			currentUrl = getMainUrl();
			setAttribute("current", currentUrl);
		}
		return currentUrl;

	}

	public String getMainUrl() {
		return EnvironmentProperties.PROPERTIES.getProperty(EnvironmentProperties.PROPERTIES.getProperty("MainUrl"));
	}

	private void setDB(int index) {
		setAttribute(ConstUtils.DB_INDEX, index);
	}

	public DataBase getDBConfig() {
		int dbIndex = (int) getAttribute(ConstUtils.DB_INDEX);
		return EnvironmentProperties.DB_CONFIGS.get(dbIndex);

	}

	private void setWebservice(int index) {
		setAttribute(ConstUtils.API_INDEX, index);
	}

	public Webservice getWebservice() {
		int dbIndex = (int) getAttribute(ConstUtils.API_INDEX);
		return EnvironmentProperties.WEBSERVICES.get(dbIndex);

	}

	@SuppressWarnings("unchecked")
	private void setBaseAttributes() {
		Enumeration<String> enums = (Enumeration<String>) EnvironmentProperties.PROPERTIES.propertyNames();
		while (enums.hasMoreElements()) {
			String key = enums.nextElement();
			String value = EnvironmentProperties.PROPERTIES.getProperty(key);
			setAttribute(key, value);
		}
	}

}
