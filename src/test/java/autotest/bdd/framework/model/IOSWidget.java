package autotest.bdd.framework.model;

public enum IOSWidget {
	TextField, StaticText, Image, ScrollView, Other;

	public static IOSWidget getIOSWidget(String name) {
		for (IOSWidget e : IOSWidget.values()) {
			if (e.toString().equalsIgnoreCase(name)) {
				return e;
			}
		}
		return null;
	}

}

