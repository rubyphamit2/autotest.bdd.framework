
package autotest.bdd.framework.model;

public enum AndroidWidget {
	Button, ImageButton, CheckBox, TextView, CheckedTextView, EditText, SwitchButton, FrameLayout, ImageView, Spinner, Switch, ProgressBar;

	public static AndroidWidget getAndroidWidget(String name) {
		for (AndroidWidget e : AndroidWidget.values()) {
			if (e.toString().equalsIgnoreCase(name)) {
				return e;
			}
		}
		return null;
	}

}
