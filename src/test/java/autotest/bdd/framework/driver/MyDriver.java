
package autotest.bdd.framework.driver;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import autotest.bdd.framework.base.EnvironmentProperties;
import autotest.bdd.framework.utils.AppContext;
import autotest.bdd.framework.utils.ConstUtils;
import autotest.bdd.framework.utils.Utils;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import net.thucydides.core.webdriver.DriverSource;

public class MyDriver implements DriverSource {
	private static final Logger LOGGER = LogManager.getLogger(MyDriver.class);

	public static final int TIMEOUT = Utils
			.toInt(EnvironmentProperties.SERENITY_PROPERTIES.getProperty("webdriver.timeouts.implicitlywait"));

	public static final DesiredCapabilities androidCapabilities = EnvironmentProperties.MAP_DESIRED_CAPABILITIES.get(ConstUtils.ANDROID);
	public static final DesiredCapabilities iosCapabilities = EnvironmentProperties.MAP_DESIRED_CAPABILITIES.get(ConstUtils.IOS);

	public static final boolean keepOldDriver = Utils
			.toBool(EnvironmentProperties.SERENITY_PROPERTIES.getProperty("serenity.driver.keepold"));

	@Override
	public WebDriver newDriver() {
		WebDriver driver = null;

		String platform = (String) AppContext.get().getAttribute(ConstUtils.WEBDRIVER);
		if (platform == null) {
			platform = System.getProperty(ConstUtils.WEBDRIVER, "chrome");
		}

		// if (keepOldDriver) {
		// driver = (WebDriver) AppContext.get().getAttribute(platform);
		// if (driver != null) {
		// return driver;
		// }
		// }else {
		//
		// closeBrowser();
		// }
		try {
			switch (platform) {
			case "firefox":
				driver = new FirefoxDriver();
				break;

			case "edge":
				driver = new EdgeDriver();
				break;

			case "iexplorer":
			case "ie":
			case "internet explorer":
				driver = new InternetExplorerDriver();
				break;
			case ConstUtils.ANDROID:
				driver = new AndroidDriver<WebElement>(new URL(androidCapabilities.getCapability("hub").toString()), androidCapabilities);
				break;
			case ConstUtils.IOS:
				driver = new IOSDriver<WebElement>(new URL(iosCapabilities.getCapability("hub").toString()), iosCapabilities);
				break;
			case "chrome":
			default:
				driver = new ChromeDriver();
				driver.manage().window().maximize();
				break;

			}
		} catch (Exception e) {
			LOGGER.error(e);
		}

		if (driver == null) {
			LOGGER.error("Could not initialize the driver for " + platform);
			System.exit(1);
		}
		AppContext.get().setAttribute(ConstUtils.DRIVER, driver);

		// if (keepOldDriver) {
		// AppContext.get().setAttribute(platform, driver);
		// }

		driver.manage().timeouts().implicitlyWait(TIMEOUT, TimeUnit.MILLISECONDS);

		return driver;
	}

	@Override
	public boolean takesScreenshots() {
		return true;
	}

}
