
package autotest.bdd.framework.base;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.webdriver.WebDriverFacade;

public class MyPageObject extends PageObject {
	private static final Logger LOGGER = LogManager.getLogger(MyPageObject.class);

	public WebDriver getProxiedDriver() {
		WebDriverFacade wdf = (WebDriverFacade) getDriver();

		return wdf.getProxiedDriver();
	}

	public void setProxiedDriver(WebDriver proxiedWebDriver) {
		WebDriverFacade wdf = (WebDriverFacade) getDriver();
		wdf.setProxiedDriver(proxiedWebDriver);
	}

	public void resetProxiedDriver() {
		setProxiedDriver(null);
		getProxiedDriver();
	}

}
