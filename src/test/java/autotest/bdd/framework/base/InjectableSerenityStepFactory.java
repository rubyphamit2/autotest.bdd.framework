package autotest.bdd.framework.base;

import java.lang.reflect.Field;
import java.util.List;

import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.steps.CandidateSteps;

import com.google.inject.Guice;
import com.google.inject.Injector;

import net.serenitybdd.jbehave.SerenityStepFactory;

public class InjectableSerenityStepFactory extends SerenityStepFactory {

	private Injector injector;

	public InjectableSerenityStepFactory(Configuration configuration, String rootPackage, ClassLoader classLoader) {
		super(configuration, rootPackage, classLoader);
		injector = Guice.createInjector();
	}

	@Override
	public List<CandidateSteps> createCandidateSteps() {
		return super.createCandidateSteps();
	}

	@Override
	public Object createInstanceOfType(Class<?> type) {
		Object instance = super.createInstanceOfType(type);
		try {
			Class<?> instanceType = type;
			while (!instanceType.equals(Object.class) && instanceType != null) {
				for (Field field : instanceType.getDeclaredFields()) {
					boolean accessible = field.isAccessible();
					field.setAccessible(true);
					Object object = field.get(instance);
					try {
						injector.injectMembers(object);
					} catch (Exception e) {
					}
					field.set(instance, object);
					field.setAccessible(accessible);
				}
				instanceType = instanceType.getSuperclass();
			}
		} catch (Exception e) {
		}
		return instance;
	}

}
