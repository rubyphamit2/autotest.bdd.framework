package autotest.bdd.framework.base;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.remote.DesiredCapabilities;

import autotest.bdd.framework.api.model.Webservice;
import autotest.bdd.framework.db.DataBase;
import autotest.bdd.framework.utils.ConstUtils;
import autotest.bdd.framework.utils.Utils;

public class EnvironmentProperties {
	private static final Logger LOGGER = LogManager.getLogger(EnvironmentProperties.class);

	private static final String PROFILE = System.getProperty("profile");

	public static final Properties ORIGINAL_SYSTEM_PROPERTIES = System.getProperties();
	public static final Properties PROPERTIES = loadProperties();
	public static final List<DataBase> DB_CONFIGS = readDBConfigs();
	public static final List<Webservice> WEBSERVICES = readWebservices();
	public static final Properties SERENITY_PROPERTIES = loadSerenityProperties();
	public static final Map<String, DesiredCapabilities> MAP_DESIRED_CAPABILITIES = loadDesiredCapabilities();
	public static final Properties OBJECT_REPOSITORY = getObjRepo();

	private EnvironmentProperties() {

	}

	private static Properties getObjRepo() {
		String folderPath = ConstUtils.BASE_DIR + File.separator + PROPERTIES.getProperty("object.repository.dir");
		return Utils.getProperties(new File(folderPath));
	}

	private static List<Webservice> readWebservices() {
		String apiconfigFile = "apiconfig.xml";
		if (StringUtils.isNotEmpty(PROFILE)) {
			apiconfigFile = PROFILE + "." + apiconfigFile;
		}
		if (!Utils.isResourceFileExist(apiconfigFile)) {
			return new ArrayList<>();
		}
		return Utils.getWebservices(apiconfigFile);
	}

	private static List<DataBase> readDBConfigs() {
		String dbConfigFile = "dbconfig.xml";
		if (StringUtils.isNotEmpty(PROFILE)) {
			dbConfigFile = PROFILE + "." + dbConfigFile;
		}
		if (!Utils.isResourceFileExist(dbConfigFile)) {
			return new ArrayList<>();
		}
		return Utils.getDbConfigs(dbConfigFile);
	}

	private static Properties loadProperties() {
		Properties properties = null;
		try {
			properties = new Properties();
			properties.load(EnvironmentProperties.class.getClassLoader().getResourceAsStream("application.properties"));
			if (StringUtils.isNoneEmpty(PROFILE)) {
				properties.load(EnvironmentProperties.class.getClassLoader().getResourceAsStream(PROFILE + ".application.properties"));
			}
		} catch (Exception e) {
			LOGGER.error("Error while loading environment property ... " + e);
			System.exit(1);
		}
		return properties;

	}

	private static Properties loadSerenityProperties() {
		String fileName = System.getProperty("properties", ConstUtils.DEFAULT_SERENITY_PROPERTIES);
		Properties properties = null;
		try {
			properties = new Properties();
			properties.load(new FileInputStream(new File(ConstUtils.BASE_DIR + File.separator + fileName)));
			properties.putAll(ORIGINAL_SYSTEM_PROPERTIES);
		} catch (Exception e) {
			LOGGER.error("Error while loading environment property ... " + e);
			System.exit(1);
		}
		return properties;
	}

	public static Map<String, String> getUser(String user) {
		Map<String, String> userinfo = new HashMap<>();
		userinfo.put("username", PROPERTIES.getProperty("users." + user + ".username"));
		userinfo.put("password", PROPERTIES.getProperty("users." + user + ".password"));
		return userinfo;
	}

	private static Map<String, DesiredCapabilities> loadDesiredCapabilities() {
		Map<String, DesiredCapabilities> mapCapabilities = new HashMap<>();
		mapCapabilities.put(ConstUtils.IOS, new DesiredCapabilities());
		mapCapabilities.put(ConstUtils.ANDROID, new DesiredCapabilities());
		@SuppressWarnings("rawtypes")
		Enumeration e = SERENITY_PROPERTIES.propertyNames();

		while (e.hasMoreElements()) {
			String key = (String) e.nextElement();
			DesiredCapabilities capabilities = null;
			if (key.startsWith(ConstUtils.IOS)) {
				capabilities = mapCapabilities.get(ConstUtils.IOS);
				capabilities.setCapability(key.replaceAll(ConstUtils.IOS + ".", ""), SERENITY_PROPERTIES.getProperty(key));
				mapCapabilities.put(ConstUtils.IOS, capabilities);

			} else if (key.startsWith(ConstUtils.ANDROID)) {
				capabilities = mapCapabilities.get(ConstUtils.ANDROID);
				capabilities.setCapability(key.replaceAll(ConstUtils.ANDROID + ".", ""), SERENITY_PROPERTIES.getProperty(key));
				mapCapabilities.put(ConstUtils.ANDROID, capabilities);
			}
		}

		return mapCapabilities;
	}

}
