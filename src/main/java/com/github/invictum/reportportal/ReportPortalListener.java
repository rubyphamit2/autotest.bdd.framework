package com.github.invictum.reportportal;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.logging.Logs;

import com.github.invictum.reportportal.handler.Handler;
import com.github.invictum.reportportal.injector.IntegrationInjector;
import com.google.inject.Inject;

import net.thucydides.core.model.DataTable;
import net.thucydides.core.model.Story;
import net.thucydides.core.model.TestOutcome;
import net.thucydides.core.steps.ExecutedStepDescription;
import net.thucydides.core.steps.StepFailure;
import net.thucydides.core.steps.StepListener;
import net.thucydides.core.webdriver.ThucydidesWebDriverSupport;

public class ReportPortalListener implements StepListener {
	private static final Logger LOGGER = LogManager.getLogger(ReportPortalListener.class);

	@Inject
	private Handler handler;

	@Inject
	private LogStorage logStorage;

	private Story story;
	private String step;

	public ReportPortalListener() {
		IntegrationInjector.getInjector().injectMembers(this);
	}

	public void testSuiteStarted(Class<?> storyClass) {
		handler.startSuite(storyClass);
	}

	public void testSuiteStarted(Story story) {
		this.story = story;
		handler.startSuite(story);
	}

	/**
	 * NhanPT2 handle test suite finished duplication cause' by Serenity
	 * Thuycydies
	 */
	public void testSuiteFinished() {
		if (this.story != null) {
			handler.finishSuite();
		}
		this.story = null;
	}

	public void testStarted(String description) {
		this.step = description;
		handler.startTest(description);
	}

	public void testStarted(String description, String id) {
		testStarted(description);
	}

	public void testFinished(TestOutcome result) {
		if (this.step != null) {
			handler.finishTest(result);
			logStorage.clean();
		}
		this.step = null;
	}

	public void testRetried() {
		/* Not used by listener */
	}

	public void stepStarted(ExecutedStepDescription description) {
		/* Not used by listener */
	}

	public void skippedStepStarted(ExecutedStepDescription description) {
		/* Not used by listener */
	}

	public void stepFailed(StepFailure failure) {
		collectDriverLogs();
	}

	public void lastStepFailed(StepFailure failure) {
		/* Not used by listener */
	}

	public void stepIgnored() {
		collectDriverLogs();
	}

	public void stepPending() {
		collectDriverLogs();
	}

	public void stepPending(String message) {
		collectDriverLogs();
	}

	public void stepFinished() {
		collectDriverLogs();
	}

	public void testFailed(TestOutcome testOutcome, Throwable cause) {
		/* Not used by listener */
	}

	public void testIgnored() {
		/* Not used by listener */
	}

	public void testSkipped() {
		/* Not used by listener */
	}

	public void testPending() {
		/* Not used by listener */
	}

	public void testIsManual() {
		/* Not used by listener */
	}

	public void notifyScreenChange() {
		/* Not used by listener */
	}

	public void useExamplesFrom(DataTable table) {
		/* Not used by listener */
	}

	public void addNewExamplesFrom(DataTable table) {
		/* Not used by listener */
	}

	public void exampleStarted(Map<String, String> data) {
		/* Not used by listener */
	}

	public void exampleFinished() {
		/* Not used by listener */
	}

	public void assumptionViolated(String message) {
		/* Not used by listener */
	}

	public void testRunFinished() {
		/* Not used by listener */
	}

	private void collectDriverLogs() {
		if (ThucydidesWebDriverSupport.isDriverInstantiated()) {
			Logs logs = ThucydidesWebDriverSupport.getDriver().manage().logs();
			logStorage.collect(logs);
		}
	}
}
